/*jshint camelcase: false */

'use strict';

module.exports = function(app, config, ejwt) {

    // app.use('/api/', ejwt({
    //         secret: config.token_secret,
    //         userProperty: 'tokenPayload'
    //     })
    //     .unless({
    //         path: [
    //             '/',
    //             '/test',
    //             '/logout',
    //             '/docs',
    //             config.api_version + '/login',
    //             config.api_version + '/useraccounts',
    //             config.api_version + '/modules',
    //             config.api_version + '/module_item',
    //             /^\/api\/1.0\/product\/.*/,
    //             /^\/api\/1.0\/searchProduct\/.*/,
    //             /^\/api\/1.0\/productCategoryFilter\/.*/,
    //             config.api_version + '/product',
    //             /^\/api\/1.0\/attributes\/.*/,
    //             config.api_version + '/attributes',
    //             /^\/api\/1.0\/productType\/.*/,
    //             config.api_version + '/productType',
    //             /^\/api\/1.0\/archiveProduct\/.*/,
    //             config.api_version + '/publishProduct',
    //             config.api_version + '/unpublishProduct',
    //             config.api_version + '/activeProduct',
    //             config.api_version + '/archivedProduct',
    //             /^\/api\/1.0\/publishProduct\/.*/,
    //             /^\/api\/1.0\/store\/.*/,
    //             config.api_version + '/store',
    //             /^\/api\/1.0\/priceHistory\/.*/,
    //             /^\/api\/1.0\/storeOwner\/.*/,
    //             config.api_version + '/priceHistory',
    //             config.api_version + '/storeOwner',
    //             /^\/api\/1.0\/supplier\/.*/,
    //             config.api_version + '/supplier',
    //             /^\/api\/1.0\/categories\/.*/,
    //             /^\/api\/1.0\/category\/.*/,
    //             config.api_version + '/categories',
    //             /^\/api\/1.0\/variant\/.*/,
    //             config.api_version + '/variant',
    //             /^\/api\/1.0\/inventory\/.*/,
    //             config.api_version + '/inventory',
    //             /^\/api\/1.0\/attributeGrp\/.*/,
    //             config.api_version + '/attributeGrp',
    //             /^\/api\/1.0\/attributeValue\/.*/,
    //             config.api_version + '/attributeValue',
    //             /^\/api\/1.0\/productCategory\/.*/,
    //             config.api_version + '/productCategory',
    //             /^\/api\/1.0\/attributeCategoryMap\/.*/,
    //             config.api_version + '/attributeCategoryMap',
    //             config.api_version + '/modules/permissions',
    //             /^\/api\/1.0\/modules\/.*/,
    //             {
    //                 url: config.api_version + '/employees',
    //                 methods: ['POST', 'OPTIONS']
    //             },
    //             {
    //                 url: config.api_version + '/fileUpload',
    //                 methods: ['POST', 'OPTIONS']
    //             }
    //         ]
    //     }));


    if (app.get('env') === 'development' || app.get('env') === 'staging') {
        app.use(function(err, req, res, next) {
            /*jshint unused: vars*/
            console.info('DEVELOPMENT / STAGING error: ');
            if (err.name === 'UnauthorizedError') {
                res.status(401).json({
                    response: {
                        result: 'UnauthorizedError',
                        success: false,
                        msg: err.inner
                    },
                    statusCode: 401
                });
            }
        });
    }



    app.use(function(err, req, res, next) {
        /*jshint unused: vars*/
        console.info('PRODUCTION error: ');
        if (err.name === 'UnauthorizedError') {
            res.status(401).json({
                response: {
                    result: 'UnauthorizedError',
                    success: false,
                    msg: err.inner
                },
                statusCode: 401
            });
        }
    });
};
