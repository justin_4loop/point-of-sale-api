/*jshint camelcase: false */

'use strict';

var env = process.env.NODE_ENV;
var config = require('../config/environment/' + env);

var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash-node');

var mysql = require('mysql');
var Database = require('../app/utils/database').Database;
var db = new Database();

module.exports = function(passport, jwt) {

    passport.use('user', new LocalStrategy(
        function(username, password, done) {
            console.log('username ' + username + ', password ' + password);
            db.query("SELECT * FROM user_accounts where username='" + username + "' limit 1", verifyAuth(password, done));
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    function verifyAuth(password, done) {

        return function(err, user) {
            console.log('->rows: ', user[0]);
            if (err) {
                console.log('->error:', err);
                return done(err, null);
            }

            if (_.isEmpty(user[0])) {
                console.log('->user is empty: ');
                return done(null, {
                    msg: 'User does not exist with this email address.',
                    success: false,
                    result: ''
                });
            } else {
                console.log('>user not empty: ');
                if (user[0].isStatus === 0) {
                    console.log('->user status : 0');
                    return done(null, {
                        msg: 'Your account is disabled.. please verify to admin',
                        success: false,
                        result: ''
                    });
                } else {
                    console.log('->user status : 1');
                    if (!bcrypt.compareSync(password, user[0].password)) {
                        console.log('->password not match with the encrypted');
                        return done(null, {
                            msg: 'Invalid Username or Password',
                            success: false,
                            result: ''
                        });
                    } else {
                        console.log('->password match with the encrypted :)');
                        var token = jwt.sign(user[0], config.token_secret, {
                            expiresIn: 3600
                        });
                        console.log('->token:', token);
                        db.query('INSERT INTO token_sessions(session_token,session_status,session_time) VALUES (\'' + token + '\',1,now())', function(err, data) {
                            //console.log('data : ', data);
                            if (!err) {
                                return done(null, {
                                    msg: 'Login successfully',
                                    success: true,
                                    result: {
                                        msg: 'success',
                                        result: user[0],
                                        token: token
                                    }
                                });
                            } else {
                                return done(null, {
                                    msg: 'errors upon token insertion',
                                    success: false,
                                    result: data
                                });
                            }

                        });

                    }


                }
            }

        };
    }
};
