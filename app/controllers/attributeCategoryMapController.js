'use strict';

var attributeCategoryMapDao = require('../daos/attributeCategoryMapDao');

function AttributeCategoryMap() {
    this.attributeCategoryMapDao = attributeCategoryMapDao;
}



AttributeCategoryMap.prototype.saveAttributeCategoryMap = function(data, next) {
    var obj = {
        fk_ptype_id: data.fk_ptype_id,
        fk_att_id: data.fk_att_id,
        fk_attgrp_id: data.fk_attgrp_id

    };

    attributeCategoryMapDao.saveAttributeCategoryMap(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

AttributeCategoryMap.prototype.getAttributeCategoryMap = function(data, next) {
    attributeCategoryMapDao.getAttributeCategoryMap(data, next);
}

AttributeCategoryMap.prototype.deleteAttributeCategoryMap = function(id, next) {
    attributeCategoryMapDao.deleteAttributeCategoryMap(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

AttributeCategoryMap.prototype.updateAttributeCategoryMap = function(id, data, next) {
    attributeCategoryMapDao.updateAttributeCategoryMap(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.AttributeCategoryMap = AttributeCategoryMap;
