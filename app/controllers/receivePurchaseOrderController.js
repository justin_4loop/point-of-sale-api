'use strict';

var receivePurchaseOrderDao = require('../daos/receivePurchaseOrderDao');
var async = require('async');
var _ = require('lodash-node');

function ReceivePurchaseOrder() {
    this.receivePurchaseOrderDao = receivePurchaseOrderDao;
}

ReceivePurchaseOrder.prototype.saveReceivePurchaseOrder = function(data, next) {

    var obj = {
        po_id: data.po_id,
        receive_number: data.receive_number,
        receive_date: data.receive_date
    };
    
    /*receivePurchaseOrderDao.saveReceivePurchaseOrder(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

async.waterfall([
        function(cb) {
            receivePurchaseOrderDao.checkReceivePurchaseOrderNumberIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Receive Purchase Order already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            receivePurchaseOrderDao.saveReceivePurchaseOrder(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Receive Purchase Order successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);


}

ReceivePurchaseOrder.prototype.getReceivePurchaseOrder = function(data, next) {
    receivePurchaseOrderDao.getReceivePurchaseOrder(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ReceivePurchaseOrder.prototype.getSpecificReceivePurchaseOrder = function(id, data, next) {
    receivePurchaseOrderDao.getSpecificReceivePurchaseOrder(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ReceivePurchaseOrder.prototype.deleteReceivePurchaseOrder = function(id, next) {
    receivePurchaseOrderDao.deleteReceivePurchaseOrder(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

ReceivePurchaseOrder.prototype.updateReceivePurchaseOrder = function(id, data, next) {
    receivePurchaseOrderDao.updateReceivePurchaseOrder(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.ReceivePurchaseOrder = ReceivePurchaseOrder;
