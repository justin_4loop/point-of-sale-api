'use strict';

var banksDao = require('../daos/banksDao');
var async = require('async');
var _ = require('lodash-node');

function Banks() {
    this.banksDao = banksDao;
}

Banks.prototype.saveBanks = function(data, next) {

    var obj = {
        name: data.name,
        address: data.address
    };
    
    /*banksDao.saveBanks(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

async.waterfall([
        function(cb) {
            banksDao.checkBankNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Bank already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            banksDao.saveBanks(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Bank successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

Banks.prototype.getBanks = function(data, next) {
    banksDao.getBanks(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Banks.prototype.getSpecificBanks = function(id, data, next) {
    banksDao.getSpecificBanks(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Banks.prototype.deleteBanks = function(id, next) {
    banksDao.deleteBanks(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Banks.prototype.updateBanks = function(id, data, next) {
    banksDao.updateBanks(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.Banks = Banks;
