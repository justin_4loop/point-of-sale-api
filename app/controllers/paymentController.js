'use strict';

var paymentDao = require('../daos/paymentDao');

function Payment() {
    this.paymentDao = paymentDao;
}

Payment.prototype.savePayment = function(data, next) {

    var obj = {
        pay_type_id: data.pay_type_id,
        credit_details: data.credit_details,
        amount_tendered: data.amount_tendered,
        paid_amount: data.paid_amount,
        or_number: data.or_number,
        attended_by: data.attended_by
    };

    
    paymentDao.savePayment(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

Payment.prototype.getPayment = function(data, next) {
    paymentDao.getPayment(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Payment.prototype.getSpecificPayment = function(id, data, next) {
    paymentDao.getSpecificPayment(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Payment.prototype.deletePayment = function(id, next) {
    paymentDao.deletePayment(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Payment.prototype.updatePayment = function(id, data, next) {
    paymentDao.updatePayment(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.Payment = Payment;
