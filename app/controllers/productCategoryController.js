'use strict';

var productCategoryDao = require('../daos/productCategoryDao');

function ProductCategory() {
    this.productCategoryDao = productCategoryDao;
}

ProductCategory.prototype.saveProductCategory = function(data, next) {
    var obj = {
        fk_product_id: data.fk_product_id,
        fk_category_id: data.fk_category_id

    };

    productCategoryDao.saveProductCategory(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

ProductCategory.prototype.getProductCategory = function(data, next) {
    productCategoryDao.getProductCategory(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ProductCategory.prototype.deleteProductCategory = function(id, next) {
    productCategoryDao.deleteProductCategory(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

ProductCategory.prototype.updateProductCategory = function(id, data, next) {
    productCategoryDao.updateProductCategory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.ProductCategory = ProductCategory;
