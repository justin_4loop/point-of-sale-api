'use strict';

var priceHistoryDao = require('../daos/priceHistoryDao');

function PriceHistory() {
    this.priceHistoryDao = priceHistoryDao;
}

PriceHistory.prototype.savePriceHistory = function(id,data, next) {
    priceHistoryDao.savePriceHistory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

PriceHistory.prototype.updatePriceHistory = function(id, data, next) {
    priceHistoryDao.updatePriceHistory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}

PriceHistory.prototype.getPriceHistory = function(data, next) {

    priceHistoryDao.getPriceHistory(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PriceHistory.prototype.getStoreOwner = function(id, next) {
    priceHistoryDao.getStoreOwner(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

/*PriceHistory.prototype.getstoreOwnerBoolean = function(id, data, next) {

    priceHistoryDao.getstoreOwnerBoolean(id, data, next);
}*/

PriceHistory.prototype.getSpecificPriceHistory = function(id, data, next) {

    priceHistoryDao.getSpecificPriceHistory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PriceHistory.prototype.deletePriceHistory = function(id, next) {
    priceHistoryDao.deletePriceHistory(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}



exports.PriceHistory = PriceHistory;
