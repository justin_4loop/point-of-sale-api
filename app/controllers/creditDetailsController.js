'use strict';

var creditDetailsDao = require('../daos/creditDetailsDao');

function CreditDetails() {
    this.creditDetailsDao = creditDetailsDao;
}

CreditDetails.prototype.saveCreditDetails = function(data, next) {

    var obj = {
        bank_id: data.bank_id,
        card_type_id: data.card_type_id,
        name_on_card: data.name_on_card,
        exp_date: data.exp_date
    };
    
    creditDetailsDao.saveCreditDetails(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

CreditDetails.prototype.getCreditDetails = function(data, next) {
    creditDetailsDao.getCreditDetails(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

CreditDetails.prototype.getSpecificCreditDetails = function(id, data, next) {
    creditDetailsDao.getSpecificCreditDetails(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

CreditDetails.prototype.deleteCreditDetails = function(id, next) {
    creditDetailsDao.deleteCreditDetails(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

CreditDetails.prototype.updateCreditDetails = function(id, data, next) {
    creditDetailsDao.updateCreditDetails(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.CreditDetails = CreditDetails;
