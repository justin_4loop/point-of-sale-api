'use strict';

var productTypeDao = require('../daos/productTypeDao');
var async = require('async');
var _ = require('lodash-node');

function ProductType() {
    this.productTypeDao = productTypeDao;
}



ProductType.prototype.saveProductType = function(data, next) {
    console.log("controller", data);

    var obj = {
        name: data.name,
        label: data.label,
        description: data.description,
        ptype_image: data.ptype_image
    };


    /*productTypeDao.saveProductType(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

async.waterfall([
        function(cb) {
            productTypeDao.checkProductTypeNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Product Type already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            productTypeDao.saveProductType(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Product Type successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);
}

ProductType.prototype.getProductType = function(data, next) {
    productTypeDao.getProductType(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ProductType.prototype.getSpecificProductType = function(id, data, next) {
    productTypeDao.getSpecificProductType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ProductType.prototype.deleteProductType = function(id, next) {

    productTypeDao.deleteProductType(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

ProductType.prototype.updateProductType = function(id, data, next) {
    productTypeDao.updateProductType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.ProductType = ProductType;
