'use strict';

var attributesDao = require('../daos/attributesDao');
var async = require('async');
var _ = require('lodash-node');

function Attributes() {
    this.attributesDao = attributesDao;
}



Attributes.prototype.saveAttributes = function(data, next) {

    console.log("AttributesController", data);

    // var obj = {
    //     name: data.name,
    //     label: data.label,
    //     type: data.type,
    //     default_value: data.default_value,
    //     val: data.val,
    //     list_value: data.list_value,
    //     sort_order: data.sort_order,
    //     is_visible: data.is_visible,
    //     is_search: data.is_search,
    //     is_variation: data.is_variation,
    //     is_required: data.is_required,

    // };

    var obj = {
        name: data.fields.name,
        label: data.fields.label,
        type: data.fields.type,
        is_visible: data.fields.is_visible,
        is_required: data.fields.is_required,
        is_variation: data.fields.is_variation,
        is_custumizable: data.fields.is_custumizable,
        is_unique: data.fields.is_unique,
        val: data.val,
        productType_id: data.fields.productType_id


        

    };

    /*attributesDao.saveAttributes(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    })*/

    async.waterfall([
        function(cb) {
            attributesDao.checkAttributesNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Attributes already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            attributesDao.saveAttributes(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Attributes successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

Attributes.prototype.getAttributes = function(data, next) {
    attributesDao.getAttributes(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Attributes.prototype.getSpecificAttributes = function(id, data, next) {

    console.log("Controller ID", id);
    attributesDao.getSpecificAttributes(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Attributes.prototype.deleteAttributes = function(id, next) {
    attributesDao.deleteAttributes(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Attributes.prototype.updateAttributes = function(id, data, next) {

    var obj = {
        name: data.fields.name,
        label: data.fields.label,
        type: data.fields.type,
        is_visible: data.fields.is_visible,
        is_required: data.fields.is_required,
        is_variation: data.fields.is_variation,
        is_custumizable: data.fields.is_custumizable,
        is_unique: data.fields.is_unique,
        val: data.val,
        productType_id: data.fields.productType_id


        

    };
    
    attributesDao.updateAttributes(id, obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.Attributes = Attributes;
