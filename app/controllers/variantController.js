'use strict';

var variantDao = require('../daos/variantDao');

function Variant() {
    this.variantDao = variantDao;
}

Variant.prototype.saveVariant = function(data, next) {


    var obj = {
        attributeValue_id: data.attributeValue_id,
        att_id: data.att_id,
        prod_id: data.prod_id,
        ph_id: data.ph_id,
        sku: data.sku,
        product_imgPath: data.product_imgPath
    };

    variantDao.saveVariant(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

Variant.prototype.getVariant = function(data, next) {
    variantDao.getVariant(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Variant.prototype.deleteVariant = function(id, next) {
    variantDao.deleteVariant(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Variant.prototype.updateVariant = function(id, data, next) {
    variantDao.updateVariant(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.Variant = Variant;
