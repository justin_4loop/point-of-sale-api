'use strict';

var adjustmentDao = require('../daos/adjustmentDao');
var async = require('async');
var _ = require('lodash-node');

function Adjustment() {
    this.adjustmentDao = adjustmentDao;
}

Adjustment.prototype.saveAdjustment = function(data, next) {

    var obj = {
        adj_number: data.adj_number,
        adj_type: data.adj_type,
        date: data.date,
        adjusted_by: data.adjusted_by
    };

    
    /*adjustmentDao.saveAdjustment(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

    async.waterfall([
        function(cb) {
            adjustmentDao.checkAdjustmentIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Adjustment already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            adjustmentDao.saveAdjustment(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Adjustment successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

Adjustment.prototype.getAdjustment = function(data, next) {
    adjustmentDao.getAdjustment(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Adjustment.prototype.getSpecificAdjustment = function(id, data, next) {
    adjustmentDao.getSpecificAdjustment(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Adjustment.prototype.deleteAdjustment = function(id, next) {
    adjustmentDao.deleteAdjustment(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Adjustment.prototype.updateAdjustment = function(id, data, next) {
    adjustmentDao.updateAdjustment(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.Adjustment = Adjustment;
