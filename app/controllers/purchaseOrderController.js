'use strict';

var purchaseOrderDao = require('../daos/purchaseOrderDao');
var async = require('async');
var _ = require('lodash-node');

function PurchaseOrder() {
    this.purchaseOrderDao = purchaseOrderDao;
}

PurchaseOrder.prototype.savePurchaseOrder = function(data, next) {

    var obj = {
        po_number: data.po_number,
        po_date: data.po_date,
        po_status: data.po_status,
        created_by: data.created_by,
        supplier_id: data.supplier_id
    };
    
    /*purchaseOrderDao.savePurchaseOrder(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

    async.waterfall([
        function(cb) {
            purchaseOrderDao.checkPurchaseOrderNumberIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Purchase Order already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            purchaseOrderDao.savePurchaseOrder(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Purchase Order successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

PurchaseOrder.prototype.getPurchaseOrder = function(data, next) {
    purchaseOrderDao.getPurchaseOrder(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PurchaseOrder.prototype.getSpecificPurchaseOrder = function(id, data, next) {
    purchaseOrderDao.getSpecificPurchaseOrder(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PurchaseOrder.prototype.deletePurchaseOrder = function(id, next) {
    purchaseOrderDao.deletePurchaseOrder(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

PurchaseOrder.prototype.updatePurchaseOrder = function(id, data, next) {
    purchaseOrderDao.updatePurchaseOrder(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.PurchaseOrder = PurchaseOrder;
