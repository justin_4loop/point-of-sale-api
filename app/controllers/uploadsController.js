'use strict';

var uploadsDao = require('../daos/uploadsDao');

function UploadAFile() {
    this.uploadsDao = uploadsDao;
}


UploadAFile.prototype.uploadAFile = function (files, next) {
    var data = {};
    data.filename = files.filename;
    data.filetype = files.mimetype;
    data.url = files.path;
    data.filesize = files.size;
    uploadsDao.uploadAFile(data,next);
};

exports.UploadFile = UploadAFile;