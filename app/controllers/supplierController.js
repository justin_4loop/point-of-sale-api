'use strict';

var supplierDao = require('../daos/supplierDao');
var async = require('async');
var _ = require('lodash-node');

function Supplier() {
    this.supplierDao = supplierDao;
}

Supplier.prototype.saveSupplier = function(data, next) {
    var obj = {
        name: data.name,
        contact_person: data.contact_person,
        contact_number: data.contact_number,
        address: data.address,
        isActive: data.isActive

    };
    /*supplierDao.saveSupplier(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }

        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

    async.waterfall([
        function(cb) {
            supplierDao.checkSupplierNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Supplier already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            supplierDao.saveSupplier(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Supplier successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);


}

Supplier.prototype.getSupplier = function(data, next) {
    supplierDao.getSupplier(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Supplier.prototype.deleteSupplier = function(id, next) {
    supplierDao.deleteSupplier(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }

        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Supplier.prototype.updateSupplier = function(id, data, next) {
    supplierDao.updateSupplier(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }

        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}





exports.Supplier = Supplier;
