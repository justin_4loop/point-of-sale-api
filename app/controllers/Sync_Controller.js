'use strict';

var Sync_Prod_Dao = require('../daos/Sync_Product_Dao');
var Sync_Emp_Dao = require('../daos/Sync_Employee_Dao');
var Sync_Helper_Dao = require('../daos/Sync_Helper_Dao');

function SyncData() {
    this.Sync_Prod_Dao = Sync_Prod_Dao;
    this.Sync_Emp_Dao = Sync_Emp_Dao;
    this.Sync_Helper_Dao = Sync_Helper_Dao;
}

/** ***************************  P R O D U C T *************************  **/

SyncData.prototype.getProduct = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getProduct(store_id, deviceName, next);
};

SyncData.prototype.getProduct_Variant = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getProduct_Variant(store_id, deviceName, next);
};

SyncData.prototype.getProductType = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getProductType(store_id, deviceName, next);
};

SyncData.prototype.getAttributeValue = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getAttributeValue(store_id, deviceName, next);
};

SyncData.prototype.getInventory = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getInventory(store_id, deviceName, next);
};

SyncData.prototype.getPriceHistory = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getPriceHistory(store_id, deviceName, next);
};

SyncData.prototype.getAttributes = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getAttributes(store_id, deviceName, next);
};

SyncData.prototype.getAttributeGroup = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getAttributeGroup(store_id, deviceName, next);
};

SyncData.prototype.getAttributeCategoryMap = function(store_id, deviceName, next) {
	Sync_Prod_Dao.getAttributeCategoryMap(store_id, deviceName, next);
};

/** *************************** E N D  P R O D U C T *************************  **/

/** set status code from Device logs to 1 from 0  AND products logs or employee logs, it depends on modlogs type**/
SyncData.prototype.UpdateProductLogsStat = function(data, store_id, deviceName, modlogs, next) {
	Sync_Helper_Dao.UpdateProductLogsStat(data, store_id, deviceName, modlogs,next);
};


/** *************************** E M P L O Y E E *************************  **/

SyncData.prototype.getStores = function(deviceName, next){
	Sync_Emp_Dao.getStores(deviceName, next);
}

SyncData.prototype.getEmployees = function(deviceName, next) {
	Sync_Emp_Dao.getEmployees(deviceName, next);
};

SyncData.prototype.getUserAccounts = function(deviceName, next) {
	Sync_Emp_Dao.getUserAccounts(deviceName, next);
};

SyncData.prototype.getUserAccess = function(deviceName, next) {
	Sync_Emp_Dao.getUserAccess(deviceName, next);
};

SyncData.prototype.getUserRoles = function(deviceName, next) {
	Sync_Emp_Dao.getUserRoles(deviceName, next);
};

SyncData.prototype.getUserPrivs = function(deviceName, next) {
	Sync_Emp_Dao.getUserPrivs(deviceName, next);
};

SyncData.prototype.getModuleItems = function(deviceName, next) {
	Sync_Emp_Dao.getModuleItems(deviceName, next);
};

SyncData.prototype.getModules = function(deviceName, next) {
	Sync_Emp_Dao.getModules(deviceName, next);
};

/** *************************** E N D  E M P L O Y E E *************************  **/

exports.SyncData = SyncData;
