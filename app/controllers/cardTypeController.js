'use strict';

var cardTypeDao = require('../daos/cardTypeDao');
var async = require('async');
var _ = require('lodash-node');

function CardType() {
    this.cardTypeDao = cardTypeDao;
}

CardType.prototype.saveCardType = function(data, next) {

    var obj = {
        name: data.name
    };
    
    /*cardTypeDao.saveCardType(obj, function(err, response) {
        if (err) {
           next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

async.waterfall([
        function(cb) {
            cardTypeDao.checkCardTypeNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Card Type already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            cardTypeDao.saveCardType(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Card Type successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

CardType.prototype.getCardType = function(data, next) {
    cardTypeDao.getCardType(data, function(err, response) {
        if (err) {
           next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

CardType.prototype.getSpecificCardType = function(id, data, next) {
    cardTypeDao.getSpecificCardType(id, data, function(err, response) {
        if (err) {
           next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

CardType.prototype.deleteCardType = function(id, next) {
    cardTypeDao.deleteCardType(id, function(err, response) {
        if (err) {
           next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

CardType.prototype.updateCardType = function(id, data, next) {
    cardTypeDao.updateCardType(id, data, function(err, response) {
        if (err) {
           next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.CardType = CardType;
