'use strict';

var storeInfoDao = require('../daos/storeInfoDao');
var async = require('async');
var _ = require('lodash-node');

function StoreInfo() {
    this.storeInfoDao = storeInfoDao;
}

StoreInfo.prototype.saveStore = function(data, next) {

    var obj = {
        name: data.name,
        address: data.address,
        tin: data.tin,
        owner_name: data.owner_name,
        telno: data.telno,
        email_address: data.email_address,
        isVat: data.isVat
    };

    /*storeInfoDao.saveStore(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

async.waterfall([
        function(cb) {
            storeInfoDao.checkStoreNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Store already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            storeInfoDao.saveStore(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Store successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

StoreInfo.prototype.getStore = function(data, next) {
    storeInfoDao.getStore(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

StoreInfo.prototype.getSpecificStore = function(id, data, next) {
    storeInfoDao.getSpecificStore(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

StoreInfo.prototype.deleteStore = function(id, next) {
    storeInfoDao.deleteStore(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

StoreInfo.prototype.updateStore = function(id, data, next) {
    storeInfoDao.updateStore(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.StoreInfo = StoreInfo;
