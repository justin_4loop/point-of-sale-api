'use strict';

var payTypeDao = require('../daos/payTypeDao');
var async = require('async');
var _ = require('lodash-node');


function PayType() {
    this.payTypeDao = payTypeDao;
}

PayType.prototype.savePayType = function(data, next) {

    var obj = {
        name: data.name,
        description: data.description,
    };

    
   /* payTypeDao.savePayType(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/
    
    async.waterfall([
        function(cb) {
            payTypeDao.checkPayTypeNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Pay Type already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            payTypeDao.savePayType(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Pay Type successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

PayType.prototype.getPayType = function(data, next) {
    payTypeDao.getPayType(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PayType.prototype.getSpecificPayType = function(id, data, next) {
    payTypeDao.getSpecificPayType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

PayType.prototype.deletePayType = function(id, next) {
    payTypeDao.deletePayType(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

PayType.prototype.updatePayType = function(id, data, next) {
    payTypeDao.updatePayType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.PayType = PayType;
