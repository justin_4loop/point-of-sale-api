'use strict';

var employeesDao = require('../daos/employeesDao');

function Employees() {
    this.employeesDao = employeesDao;
}

Employees.prototype.createEmployee = function(data, next) {
    employeesDao.createEmployee(data, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'employee successfully created',
                result: returnedData
            });
        } else {
        	next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        
    });
}

Employees.prototype.getEmployee = function(next) {
    employeesDao.getEmployee(function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'successfully fetch all employees',
                result: returnedData
            });
        } else if(!err && returnedData.length < 1) {
        	next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        }
        else {
        	next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
}

Employees.prototype.getEmployeeWithoutAccount = function(next) {
    employeesDao.getEmployeeWithoutAccount(function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'successfully fetch all employees without account',
                result: returnedData
            });
        } else if(!err && returnedData.length < 1) {
            next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        }
        else {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
}

Employees.prototype.getEmployeeById = function(id, next) {
    employeesDao.getEmployeeById(id, function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'successfully fetch employee by ID',
                result: returnedData
            });
        } else if(!err && returnedData.length < 1) {
        	next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        } else {
        	next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
}

Employees.prototype.deleteEmployee = function(id, next) {
    employeesDao.deleteEmployee(id, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'successfully deleted employee',
                result: 'deleted employee_id : '+id
            });
        } else {
        	next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
}

Employees.prototype.updateEmployee = function(id, data, next) {
    employeesDao.updateEmployee(id, data, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'successfully updated employee',
                result: data
            });
        } else {
        	next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
}

exports.Employees = Employees;
