'use strict';

var attributeValueDao = require('../daos/attributeValueDao');

function AttributeValue() {
    this.attributeValueDao = attributeValueDao;
}



AttributeValue.prototype.saveAttributeValue = function(data, next) {

    var obj = {
        product_id: data.product_id,
        attribute_id: data.attribute_id,
        priceHistory_id: data.priceHistory_id,
        value: data.value,
        sku: data.sku

    };

    attributeValueDao.saveAttributeValue(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

AttributeValue.prototype.getAttributeValue = function(data, next) {
    attributeValueDao.getAttributeValue(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AttributeValue.prototype.deleteAttributeValue = function(id, next) {
    attributeValueDao.deleteAttributeValue(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

AttributeValue.prototype.updateAttributeValue = function(id, data, next) {
    attributeValueDao.updateAttributeValue(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.AttributeValue = AttributeValue;
