'use strict';

var productsDao = require('../daos/productsDao');
var functions = require('../../app/utils/functions');

function Products() {
    this.productsDao = productsDao;
}

Products.prototype.saveProduct = function(data, next) {
    productsDao.saveProduct(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, response);
    });
}

Products.prototype.getProducts = function(next) {
    productsDao.getProducts(function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.getProduct = function(id, next) {
    productsDao.getProduct(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        response.is_published = functions.IntToBoolean(response.is_published);
        response.is_published = functions.IntToBoolean(response.is_published);

        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.deleteProduct = function(id, next) {

    productsDao.deleteProduct(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Products.prototype.updateProduct = function(id, data, next) {
    productsDao.updateProduct(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}

Products.prototype.searchProduct = function(data, next) {
    // console.log(data);
    console.log("Controller Search");
    productsDao.searchProduct(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}


Products.prototype.productCategoryFilter = function(data, next) {

    productsDao.productCategoryFilter(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.archiveProduct = function(id, data, next) {
    console.log("Controller");

    productsDao.archiveProduct(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}

Products.prototype.publishProduct = function(id, data, next) {

    productsDao.publishProduct(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}

Products.prototype.getPublish = function(data, next) {
    productsDao.getPublish(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.getUnpublish = function(data, next) {
    productsDao.getUnpublish(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.getActive = function(data, next) {
    productsDao.getActive(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Products.prototype.getArchive = function(data, next) {
    productsDao.getArchive(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}





exports.Products = Products;
