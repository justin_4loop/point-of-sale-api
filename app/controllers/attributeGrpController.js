'use strict';

var attributeGrpDao = require('../daos/attributeGrpDao');
var async = require('async');
var _ = require('lodash-node');

function AttributeGrp() {
    this.attributeGrpDao = attributeGrpDao;
}



AttributeGrp.prototype.saveAttributeGrp = function(data, next) {
    var obj = {
        name: data.name,
        label: data.label,
        description: data.description,
        sort_order: data.sort_order

    };

    /*attributeGrpDao.saveAttributeGrp(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/


    async.waterfall([
        function(cb) {
            attributeGrpDao.checkAttributeGrpIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Attribute Group already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            attributeGrpDao.saveAttributeGrp(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Attribute Group successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

AttributeGrp.prototype.getAttributeGrp = function(data, next) {
    attributeGrpDao.getAttributeGrp(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AttributeGrp.prototype.deleteAttributeGrp = function(id, next) {
    attributeGrpDao.deleteAttributeGrp(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

AttributeGrp.prototype.updateAttributeGrp = function(id, data, next) {
    attributeGrpDao.updateAttributeGrp(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.AttributeGrp = AttributeGrp;
