'use strict';

var userGroupDao = require('../daos/userGroupsDao');
var userGroupPermissionDao = require('../daos/modulesDao');
var async = require('async');
var _ = require('lodash-node');


function UserGroups() {
    this.userGroupDao = userGroupDao;
}

function userGroupPermission() {
    this.userGroupPermissionDao = userGroupPermissionDao;
}

UserGroups.prototype.createUserGroup = function(data, next) {
    async.waterfall([
        function(cb) {
            userGroupDao.checkGroupNameIfExist(data, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'User Group already existed',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            userGroupDao.createUserGroup(data, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'User Group successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                }
            });
        }
    ], next);
};

UserGroups.prototype.getAllUserGroups = function(next) {
    userGroupDao.getAllUserGroups(function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'Successfully fetch all user group',
                result: returnedData
            });
        } else if (!err && returnedData.length < 1) {
            next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
    });
};

UserGroups.prototype.getUserGroupById = function(id, next) {
    userGroupDao.getUserGroupById(id, function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'Successfully fetch user group by ID',
                result: returnedData
            });
        } else if (!err && returnedData.length < 1) {
            next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
}

UserGroups.prototype.deleteUserGroup = function(id, next) {
    userGroupDao.deleteUserGroup(id, function(err, returnedData) {
        console.log('err: ', err);
        console.log('returnedData: ', returnedData);
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                success: true,
                msg: 'Successfully deleted user group',
                result: returnedData
            });

        }
    });
};

UserGroups.prototype.updateUserGroup = function(id, data, next) {
    userGroupDao.updateUserGroup(id, data, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'Successfully updated user group',
                result: data
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
};

UserGroups.prototype.setGroupPrivilege = function(role_id, data, next) {
    userGroupDao.setGroupPrivilege(role_id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        next(null, {
            success: true,
            msg: 'Permissions successfully updated',
            result: response
        });
    });
};


userGroupPermission.prototype.generateAccessPermission = function(id, next) {
    userGroupPermissionDao.generateAccessPermission(id, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: '',
                result: returnedData
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
};

exports.UserGroups = UserGroups;
exports.userGroupPermission = userGroupPermission;