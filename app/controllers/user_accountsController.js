'use strict';

var userDao = require('../daos/user_accountsDao');
var async = require('async');
var _ = require('lodash-node');

function Users() {
    this.userDao = userDao;
}

Users.prototype.createUser = function(data, next) {
    async.waterfall([
        function(callback) {
            userDao.checkEmployeeIfExist(data.emp_id, function(err, returnedData) {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                } else if (!_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'The selected employee has already an account.',
                        result: returnedData
                    });
                } else {
                    console.log('callback(null, true);');
                    callback(null, true);
                }
            });
        },
        function(datas, callback) {
            userDao.checkUserNameIfExist(data.username, function(err, returnedData) {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                } else if (!_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Username already existed. Please enter another username',
                        result: returnedData
                    });
                } else {
                    callback(null, true);
                }
            });
        },
        function(datas, callback) {
            console.log('createUser');
            userDao.createUser(data, function(err, returnedData) {
                if (!err) {
                    callback(null, {
                        success: true,
                        msg: 'user acount successfully created',
                        result: returnedData
                    });
                } else {
                    next(err, null);
                }
            });
        }
    ], next);
}

Users.prototype.getUser = function(next) {
    userDao.getUser(function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'successfully fetch all user',
                result: returnedData
            });
        } else if (!err && returnedData.length < 1) {
            next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
}

Users.prototype.getUserById = function(id, next) {
    userDao.getUserById(id, function(err, returnedData) {
        if (!err && returnedData.length > 0) {
            next(null, {
                success: true,
                msg: 'Successfully fetch user by ID',
                result: returnedData
            });
        } else if (!err && returnedData.length < 1) {
            next(null, {
                success: true,
                msg: 'query returns no record',
                result: returnedData
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
}

Users.prototype.deleteUser = function(id, next) {
    userDao.deleteUser(id, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'Successfully deleted user',
                result: 'deleted user_id : ' + id
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
}

Users.prototype.updateUser = function(id, data, next) {
    userDao.updateUser(id, data, function(err, returnedData) {
        if (!err) {
            next(null, {
                success: true,
                msg: 'Successfully updated user',
                result: data
            });
        } else {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
    });
}

exports.Users = Users;