'use strict';

var adjustmentTypeDao = require('../daos/adjustmentTypeDao');
var async = require('async');
var _ = require('lodash-node');

function AdjustmentType() {
    this.adjustmentTypeDao = adjustmentTypeDao;
}

AdjustmentType.prototype.saveAdjustmentType = function(data, next) {

    var obj = {
        name: data.name,
        description: data.description
    };

    
    /*adjustmentTypeDao.saveAdjustmentType(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

    async.waterfall([
        function(cb) {
            adjustmentTypeDao.checkAdjustmentTypeIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Adjustment Type already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            adjustmentTypeDao.saveAdjustmentType(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Adjustment Type successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

AdjustmentType.prototype.getAdjustmentType = function(data, next) {
    adjustmentTypeDao.getAdjustmentType(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AdjustmentType.prototype.getSpecificAdjustmentType = function(id, data, next) {
    adjustmentTypeDao.getSpecificAdjustmentType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AdjustmentType.prototype.deleteAdjustmentType = function(id, next) {
    adjustmentTypeDao.deleteAdjustmentType(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

AdjustmentType.prototype.updateAdjustmentType = function(id, data, next) {
    adjustmentTypeDao.updateAdjustmentType(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.AdjustmentType = AdjustmentType;
