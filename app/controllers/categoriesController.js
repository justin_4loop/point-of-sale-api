'use strict';

var categoriesDao = require('../daos/categoriesDao');
var async = require('async');
var _ = require('lodash-node');

function Categories() {
    this.categoriesDao = categoriesDao;
}



Categories.prototype.saveCategories = function(data, next) {
    console.log("Contoller", data);
    var obj = {
        cat_id: data.cat_id,
        name: data.name,
        label: data.label,
        description: data.description,
        c_image: data.c_image
    };

    /*categoriesDao.saveCategories(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });*/

     async.waterfall([
        function(cb) {
            categoriesDao.checkCategoryNameIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Category already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            categoriesDao.saveCategories(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Category successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);

}

Categories.prototype.getChildCategories = function(id, data, next) {
    categoriesDao.getChildCategories(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Categories.prototype.getSpecificCategory = function(id, data, next) {
    categoriesDao.getSpecificCategory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Categories.prototype.getCategories = function(data, next) {
    console.log("Contoller Catgories");
    categoriesDao.getCategories(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Categories.prototype.deleteCategories = function(id, next) {
    categoriesDao.deleteCategories(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Categories.prototype.updateCategories = function(id, data, next) {
    categoriesDao.updateCategories(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.Categories = Categories;
