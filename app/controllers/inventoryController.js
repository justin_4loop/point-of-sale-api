'use strict';

var inventoryDao = require('../daos/inventoryDao');

function Inventory() {
    this.inventoryDao = inventoryDao;
}



Inventory.prototype.saveInventory = function(data, next) {

    var obj = {
        fk_store_id: data.fk_store_id,
        fk_p_id: data.fk_p_id,
        status: data.status,
        stock_onhand: data.stock_onhand,
        reorder_level: data.reorder_level,
        outOfStock_reason: data.outOfStock_reason,
        reStock_date: data.reStock_date
    };

    inventoryDao.saveInventory(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

Inventory.prototype.getInventory = function(data, next) {
    inventoryDao.getInventory(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

Inventory.prototype.deleteInventory = function(id, next) {
    inventoryDao.deleteInventory(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

Inventory.prototype.updateInventory = function(id, data, next) {
    inventoryDao.updateInventory(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}



exports.Inventory = Inventory;
