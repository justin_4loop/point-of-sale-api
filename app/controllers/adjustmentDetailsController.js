'use strict';

var adjustmentDetailsDao = require('../daos/adjustmentDetailsDao');

function AdjustmentDetails() {
    this.adjustmentDetailsDao = adjustmentDetailsDao;
}

AdjustmentDetails.prototype.saveAdjustmentDetails = function(data, next) {

    var obj = {
        adjustment_id: data.adjustment_id,
        pay_id: data.pay_id,
        unit_id: data.unit_id,
        amount: data.amount,
        quantity: data.quantity
    };

    
    adjustmentDetailsDao.saveAdjustmentDetails(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });

    
   /* async.waterfall([
        function(cb) {
            adjustmentDetailsDao.checkAdjustmentDetailsIfExist(obj, function(err, returnedData) {
                if (!err && !_.isEmpty(returnedData)) {
                    next(null, {
                        success: false,
                        msg: 'Adjustment Details already exists',
                        result: returnedData
                    });
                } else if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    cb(null, true);
                }
            });
        },
        function(datas, cb) {
            adjustmentDetailsDao.saveAdjustmentDetails(obj, function(err, returnedData) {
                if (!err) {
                    next(null, {
                        success: true,
                        msg: 'Adjustment Details successfully created',
                        result: returnedData
                    });
                } else {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }
            });
        }
    ], next);*/

}

AdjustmentDetails.prototype.getAdjustmentDetails = function(data, next) {
    adjustmentDetailsDao.getAdjustmentDetails(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AdjustmentDetails.prototype.getSpecificAdjustmentDetails = function(id, data, next) {
    adjustmentDetailsDao.getSpecificAdjustmentDetails(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

AdjustmentDetails.prototype.deleteAdjustmentDetails = function(id, next) {
    adjustmentDetailsDao.deleteAdjustmentDetails(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

AdjustmentDetails.prototype.updateAdjustmentDetails = function(id, data, next) {
    adjustmentDetailsDao.updateAdjustmentDetails(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.AdjustmentDetails = AdjustmentDetails;
