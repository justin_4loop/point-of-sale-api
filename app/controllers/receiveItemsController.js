'use strict';

var receiveItemsDao = require('../daos/receiveItemsDao');

function ReceiveItems() {
    this.receiveItemsDao = receiveItemsDao;
}

ReceiveItems.prototype.saveReceiveItems = function(data, next) {

    var obj = {
        r_id: data.r_id,
        unit_id: data.unit_id,
        quantity: data.quantity,
        received_by: data.received_by
    };

    receiveItemsDao.saveReceiveItems(obj, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Saved',
            success: true
        });
    });
}

ReceiveItems.prototype.getReceiveItems = function(data, next) {
    receiveItemsDao.getReceiveItems(data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ReceiveItems.prototype.getSpecificReceiveItems = function(id, data, next) {
    receiveItemsDao.getSpecificReceiveItems(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: '',
            success: true
        });
    });
}

ReceiveItems.prototype.deleteReceiveItems = function(id, next) {
    receiveItemsDao.deleteReceiveItems(id, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Deleted',
            success: true
        });
    });
}

ReceiveItems.prototype.updateReceiveItems = function(id, data, next) {
    receiveItemsDao.updateReceiveItems(id, data, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }
        next(null, {
            result: response,
            msg: 'Record Successfully Updated',
            success: true
        });
    });
}


exports.ReceiveItems = ReceiveItems;
