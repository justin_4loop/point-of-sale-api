'use strict';
var Sync = require('./routing/Sync_Route');

module.exports = function(app, config) {
	
	/** ***************************  P R O D U C T *************************  **/
	app.route(config.api_version + '/syncproduct/:store_id/:devicename').get(Sync.getProduct);
	app.route(config.api_version + '/syncproductvariant/:store_id/:devicename').get(Sync.getProduct_Variant);
	app.route(config.api_version + '/syncproducttype/:store_id/:devicename').get(Sync.getProductType);
	app.route(config.api_version + '/syncattributevalue/:store_id/:devicename').get(Sync.getAttributeValue);
	app.route(config.api_version + '/syncinventory/:store_id/:devicename').get(Sync.getInventory);
	app.route(config.api_version + '/syncpricehistory/:store_id/:devicename').get(Sync.getPriceHistory);
	app.route(config.api_version + '/syncattributes/:store_id/:devicename').get(Sync.getAttributes);
	app.route(config.api_version + '/syncattributegroup/:store_id/:devicename').get(Sync.getAttributeGroup);
	app.route(config.api_version + '/syncattributecategorymap/:store_id/:devicename').get(Sync.getAttributeCategoryMap);
	app.route(config.api_version + '/syncProductImages/:store_id/:devicename')
		.get(function(req, res) { res.json(config.api_host_url + "/public/tmp/" + req.params.devicename+ ".zip");});

	/** *************************** E N D  P R O D U C T *************************  **/


	/** set status code from Device logs to 1 from 0  AND products logs or employee logs, it depends on modlogs type**/
	app.route(config.api_version + '/syncupdatestatus/:store_id/:devicename/:modlogs')
		.post(Sync.UpdateProductLogsStat);


	/** *************************** E M P L O Y E E *************************  **/

	app.route(config.api_version + '/syncstores/:devicename').get(Sync.getStores);
	app.route(config.api_version + '/syncemployees/:devicename').get(Sync.getEmployees);
	app.route(config.api_version + '/syncuseraccounts/:devicename').get(Sync.getUserAccounts);
	app.route(config.api_version + '/syncuserroles/:devicename').get(Sync.getUserRoles);
	app.route(config.api_version + '/syncuseraccess/:devicename').get(Sync.getUserAccess);
	app.route(config.api_version + '/synmodules/:devicename').get(Sync.getModules);
	app.route(config.api_version + '/syncmoduleitems/:devicename').get(Sync.getModuleItems);
	app.route(config.api_version + '/syncuserprivs/:devicename').get(Sync.getUserPrivs);

	/** *************************** E N D  E M P L O Y E E *************************  **/


	app.route(config.api_version + '/syncproduct/:store_id/:devicename')
		.get(Sync.getProduct);

	app.route(config.api_version + '/syncproductcallback/:store_id/:devicename')
		.get(Sync.getProduct_Callback);

	//GET PRODUCT IMAGES
	app.route(config.api_version + '/syncproductimage/:devicename')
		.get(Sync.getProductImages);

	// GET EMPLOYEES
	app.route(config.api_version + '/syncmeployeeinfo/:devicename')
		.get(Sync.getEmployeeInfo);

	// GET MODULES
	app.route(config.api_version + '/syncmodulepriv')
		.get(Sync.getModulePriv);

	//GET BANKS
	app.route(config.api_version + '/syncbank/:store_id')
		.get(Sync.getBankCredit);
};