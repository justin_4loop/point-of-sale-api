'use strict';

var cb = require('./../../utils/callback');

var productsCtrl = require('../../controllers/productsController').Products;
var products = new productsCtrl();
var multer = require('multer');


exports.saveProduct = function onRequest(req, res) {
    //code goes here
    products.saveProduct(req.body, cb.setupResponseCallback(res));
};


exports.getProducts = function onRequest(req, res) {
    //code goes here
    products.getProducts(cb.setupResponseCallback(res));
};


exports.getProduct = function onRequest(req, res) {
    //code goes here
    products.getProduct(req.params.id, cb.setupResponseCallback(res));
};


exports.deleteProduct = function onRequest(req, res) {
    //code goes here

    products.deleteProduct(req.params.id, cb.setupResponseCallback(res));
};

exports.updateProduct = function onRequest(req, res) {
    //code goes here

    products.updateProduct(req.params.id, req.body, cb.setupResponseCallback(res));

};


exports.searchProduct = function onRequest(req, res) {
    console.log("Routing Search");

    // console.log(req.params.productName);

    products.searchProduct(req.params.productName, cb.setupResponseCallback(res));

};

exports.productCategoryFilter = function onRequest(req, res) {

    products.productCategoryFilter(req.params.id, cb.setupResponseCallback(res));

};

exports.archiveProduct = function onRequest(req, res) {
    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/

    // console.log("req.params.id", req.params.id);
    console.log("Routing");


    products.archiveProduct(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.publishProduct = function onRequest(req, res) {

    products.publishProduct(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.getPublish = function onRequest(req, res) {
    console.log("getPublish");

    products.getPublish(req.body, cb.setupResponseCallback(res));

};

exports.getUnpublish = function onRequest(req, res) {

    products.getUnpublish(req.body, cb.setupResponseCallback(res));

};

exports.getActive = function onRequest(req, res) {

    products.getActive(req.body, cb.setupResponseCallback(res));

};

exports.getArchive = function onRequest(req, res) {

    products.getArchive(req.body, cb.setupResponseCallback(res));

};
