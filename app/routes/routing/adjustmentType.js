'use strict';

var cb = require('./../../utils/callback');
var adjustmentTypeCtrl = require('../../controllers/adjustmentTypeController').AdjustmentType;
var adjustmentType = new adjustmentTypeCtrl();





exports.saveAdjustmentType = function onRequest(req, res) {
    //code goes here
    adjustmentType.saveAdjustmentType(req.body, cb.setupResponseCallback(res));
};

exports.getAdjustmentType = function onRequest(req, res) {
    //code goes here
    adjustmentType.getAdjustmentType(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificAdjustmentType = function onRequest(req, res) {
    //code goes here
    adjustmentType.getSpecificAdjustmentType(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteAdjustmentType = function onRequest(req, res) {
    //code goes here
    adjustmentType.deleteAdjustmentType(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAdjustmentType = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    adjustmentType.updateAdjustmentType(req.params.id, req.body, cb.setupResponseCallback(res));

};
