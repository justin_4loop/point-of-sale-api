'use strict';

var cb = require('./../../utils/callback');

var employeesCtrl = require('../../controllers/employeesController').Employees;
var employees = new employeesCtrl();

exports.createEmployee = function onRequest(req, res) {
    employees.createEmployee(req.body, cb.setupResponseCallback(res));
};

exports.getEmployee = function onRequest(req, res) {
    employees.getEmployee(cb.setupResponseCallback(res));
};

exports.getEmployeeWithoutAccount = function onRequest(req, res) {
    employees.getEmployeeWithoutAccount(cb.setupResponseCallback(res));
};

exports.getEmployeeById = function onRequest(req, res) {
    employees.getEmployeeById(req.params.id,cb.setupResponseCallback(res));
};

exports.deleteEmployee = function onRequest(req, res) {
    employees.deleteEmployee(req.params.id, cb.setupResponseCallback(res));
};

exports.updateEmployee = function onRequest(req, res) {
    employees.updateEmployee(req.params.id,req.body, cb.setupResponseCallback(res));
};