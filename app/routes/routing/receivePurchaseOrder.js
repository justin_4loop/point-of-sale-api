'use strict';

var cb = require('./../../utils/callback');
var receivePurchaseOrderCtrl = require('../../controllers/receivePurchaseOrderController').ReceivePurchaseOrder;
var receivePurchaseOrder = new receivePurchaseOrderCtrl();





exports.saveReceivePurchaseOrder = function onRequest(req, res) {
    //code goes here
    receivePurchaseOrder.saveReceivePurchaseOrder(req.body, cb.setupResponseCallback(res));
};

exports.getReceivePurchaseOrder = function onRequest(req, res) {
    //code goes here
    receivePurchaseOrder.getReceivePurchaseOrder(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificReceivePurchaseOrder = function onRequest(req, res) {
    //code goes here
    receivePurchaseOrder.getSpecificReceivePurchaseOrder(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteReceivePurchaseOrder = function onRequest(req, res) {
    //code goes here
    receivePurchaseOrder.deleteReceivePurchaseOrder(req.params.id, cb.setupResponseCallback(res));
};

exports.updateReceivePurchaseOrder = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    receivePurchaseOrder.updateReceivePurchaseOrder(req.params.id, req.body, cb.setupResponseCallback(res));

};
