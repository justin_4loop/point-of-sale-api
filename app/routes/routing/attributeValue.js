'use strict';

var cb = require('./../../utils/callback');

var attributeValueCtrl = require('../../controllers/attributeValueController').AttributeValue;
var attributeValue = new attributeValueCtrl();





exports.saveAttributeValue = function onRequest(req, res) {

    //code goes here
    attributeValue.saveAttributeValue(req.body, cb.setupResponseCallback(res));
};

exports.getAttributeValue = function onRequest(req, res) {
    //code goes here
    attributeValue.getAttributeValue(req.body, cb.setupResponseCallback(res));
};

exports.deleteAttributeValue = function onRequest(req, res) {
    //code goes here
    attributeValue.deleteAttributeValue(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAttributeValue = function onRequest(req, res) {
    //code goes here
    attributeValue.updateAttributeValue(req.params.id, req.body, cb.setupResponseCallback(res));
};
