'use strict';

var cb = require('./../../utils/callback');

var categoriesCtrl = require('../../controllers/categoriesController').Categories;
var categories = new categoriesCtrl();
var multer = require('multer');




exports.saveCategories = function onRequest(req, res) {
    console.log("Routing body", req.body);
    console.log("Routing file", req.file);


    // var storage = multer.diskStorage({
    //     destination: function(req, file, cb) {
    //         cb(null, 'public/uploads/img')
    //     },
    //     filename: function(req, file, cb) {
    //         cb(null, file.originalname);
    //     }
    // })

    // var uploadImg = multer({
    //     storage: storage
    // }).single('c_image');
    // uploadImg(req, res, function(err) {

    //     if (err) {
    //         // An error occurred when uploading
    //         // console.log(err);
    //         return
    //     }

        if (req.file) {
            // req.body.c_image = req.file.destination + '/' + req.file.originalname;
            req.body.c_image = req.file.destination + '/' + req.file.filename;
            categories.saveCategories(req.body, cb.setupResponseCallback(res));

        } else {


            categories.saveCategories(req.body, cb.setupResponseCallback(res));
        }

    // })


};

exports.getChildCategories = function onRequest(req, res) {
    //code goes here
    categories.getChildCategories(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.getSpecificCategory = function onRequest(req, res) {
    //code goes here
    categories.getSpecificCategory(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.getCategories = function onRequest(req, res) {
    //code goes here
    categories.getCategories(req.body, cb.setupResponseCallback(res));
};

exports.deleteCategories = function onRequest(req, res) {
    //code goes here
    categories.deleteCategories(req.params.id, cb.setupResponseCallback(res));
};

exports.updateCategories = function onRequest(req, res) {
    //code goes here

    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            console.log("file", file);
            cb(null, 'public/uploads/img')
        },
        filename: function(req, file, cb) {
            cb(null, file.filename);
        }
    })

    var uploadImg = multer({
        storage: storage
    }).single('c_image');
    uploadImg(req, res, function(err) {

        if (err) {
            // An error occurred when uploading
            console.log(err);
            return
        }

        if (req.file) {
            // req.body.c_image = req.file.destination + '/' + req.file.originalname;
            req.body.c_image = req.file.destination + '/' + req.file.filename;

            categories.updateCategories(req.params.id, req.body, cb.setupResponseCallback(res));

        } else {

            categories.updateCategories(req.params.id, req.body, cb.setupResponseCallback(res));
        }


    })

};
