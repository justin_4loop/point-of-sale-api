'use strict';

var cb = require('./../../utils/callback');

var attributeGrpCtrl = require('../../controllers/attributeGrpController').AttributeGrp;
var attributeGrp = new attributeGrpCtrl();





exports.saveAttributeGrp = function onRequest(req, res) {

    //code goes here
    attributeGrp.saveAttributeGrp(req.body, cb.setupResponseCallback(res));
};

exports.getAttributeGrp = function onRequest(req, res) {
    //code goes here
    attributeGrp.getAttributeGrp(req.body, cb.setupResponseCallback(res));
};

exports.deleteAttributeGrp = function onRequest(req, res) {
    //code goes here
    attributeGrp.deleteAttributeGrp(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAttributeGrp = function onRequest(req, res) {
    //code goes here
    attributeGrp.updateAttributeGrp(req.params.id, req.body, cb.setupResponseCallback(res));
};
