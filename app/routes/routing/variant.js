'use strict';

var cb = require('./../../utils/callback');

var variantCtrl = require('../../controllers/variantController').Variant;
var variant = new variantCtrl();
var multer = require('multer');






exports.saveVariant = function onRequest(req, res) {


    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'public/uploads/img/variant')
        },
        filename: function(req, file, cb) {
            cb(null, file.originalname);
        }
    })

    var uploadImg = multer({
        storage: storage
    }).single('product_imgPath');
    uploadImg(req, res, function(err) {
        // console.log(req);

        if (err) {
            // An error occurred when uploading
            console.log(err);
            return
        }


        req.body.product_imgPath = req.file.destination + '/' + req.file.originalname;


        variant.saveVariant(req.body, cb.setupResponseCallback(res));

    })

};

exports.getVariant = function onRequest(req, res) {
    //code goes here
    variant.getVariant(req.body, cb.setupResponseCallback(res));
};

exports.deleteVariant = function onRequest(req, res) {
    //code goes here
    variant.deleteVariant(req.params.id, cb.setupResponseCallback(res));
};

exports.updateVariant = function onRequest(req, res) {
    //code goes here

    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'public/uploads/img/variant')
        },
        filename: function(req, file, cb) {
            cb(null, file.originalname);
        }
    })

    var uploadImg = multer({
        storage: storage
    }).single('product_imgPath');
    uploadImg(req, res, function(err) {
        // console.log(req);

        if (err) {
            // An error occurred when uploading
            console.log(err);
            return
        }
        req.body.product_imgPath = req.file.destination + '/' + req.file.originalname;


        variant.updateVariant(req.params.id, req.body, cb.setupResponseCallback(res));

    })
};
