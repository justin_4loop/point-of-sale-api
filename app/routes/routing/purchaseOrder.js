'use strict';

var cb = require('./../../utils/callback');
var purchaseOrderCtrl = require('../../controllers/purchaseOrderController').PurchaseOrder;
var purchaseOrder = new purchaseOrderCtrl();





exports.savePurchaseOrder = function onRequest(req, res) {
    //code goes here
    purchaseOrder.savePurchaseOrder(req.body, cb.setupResponseCallback(res));
};

exports.getPurchaseOrder = function onRequest(req, res) {
    //code goes here
    purchaseOrder.getPurchaseOrder(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificPurchaseOrder = function onRequest(req, res) {
    //code goes here
    purchaseOrder.getSpecificPurchaseOrder(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deletePurchaseOrder = function onRequest(req, res) {
    //code goes here
    purchaseOrder.deletePurchaseOrder(req.params.id, cb.setupResponseCallback(res));
};

exports.updatePurchaseOrder = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    purchaseOrder.updatePurchaseOrder(req.params.id, req.body, cb.setupResponseCallback(res));



};
