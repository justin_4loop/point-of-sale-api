'use strict';

var cb = require('./../../utils/callback');
var paymentCtrl = require('../../controllers/paymentController').Payment;
var payment = new paymentCtrl();





exports.savePayment = function onRequest(req, res) {
    //code goes here
    payment.savePayment(req.body, cb.setupResponseCallback(res));
};

exports.getPayment = function onRequest(req, res) {
    //code goes here
    payment.getPayment(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificPayment = function onRequest(req, res) {
    //code goes here
    payment.getSpecificPayment(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deletePayment = function onRequest(req, res) {
    //code goes here
    payment.deletePayment(req.params.id, cb.setupResponseCallback(res));
};

exports.updatePayment = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    payment.updatePayment(req.params.id, req.body, cb.setupResponseCallback(res));

};
