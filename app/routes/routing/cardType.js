'use strict';

var cb = require('./../../utils/callback');
var cardTypeCtrl = require('../../controllers/cardTypeController').CardType;
var cardType = new cardTypeCtrl();





exports.saveCardType = function onRequest(req, res) {
    //code goes here
    cardType.saveCardType(req.body, cb.setupResponseCallback(res));
};

exports.getCardType = function onRequest(req, res) {
    //code goes here
    cardType.getCardType(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificCardType = function onRequest(req, res) {
    //code goes here
    cardType.getSpecificCardType(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteCardType = function onRequest(req, res) {
    //code goes here
    cardType.deleteCardType(req.params.id, cb.setupResponseCallback(res));
};

exports.updateCardType = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    cardType.updateCardType(req.params.id, req.body, cb.setupResponseCallback(res));

};
