'use strict';

var cb = require('./../../utils/callback');
var creditDetailsCtrl = require('../../controllers/creditDetailsController').CreditDetails;
var creditDetails = new creditDetailsCtrl();





exports.saveCreditDetails = function onRequest(req, res) {
    //code goes here
    creditDetails.saveCreditDetails(req.body, cb.setupResponseCallback(res));
};

exports.getCreditDetails = function onRequest(req, res) {
    //code goes here
    creditDetails.getCreditDetails(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificCreditDetails = function onRequest(req, res) {
    //code goes here
    creditDetails.getSpecificCreditDetails(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteCreditDetails = function onRequest(req, res) {
    //code goes here
    creditDetails.deleteCreditDetails(req.params.id, cb.setupResponseCallback(res));
};

exports.updateCreditDetails = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    creditDetails.updateCreditDetails(req.params.id, req.body, cb.setupResponseCallback(res));

};
