'use strict';

var cb = require('./../../utils/callback');

var productCategoryCtrl = require('../../controllers/productCategoryController').ProductCategory;
var productCategory = new productCategoryCtrl();





exports.saveProductCategory = function onRequest(req, res) {

    // console.log(req.body);
    //code goes here
    productCategory.saveProductCategory(req.body, cb.setupResponseCallback(res));
};

exports.getProductCategory = function onRequest(req, res) {
    //code goes here
    productCategory.getProductCategory(req.body, cb.setupResponseCallback(res));
};

exports.deleteProductCategory = function onRequest(req, res) {
    //code goes here
    productCategory.deleteProductCategory(req.params.id, cb.setupResponseCallback(res));
};

exports.updateProductCategory = function onRequest(req, res) {
    //code goes here
    productCategory.updateProductCategory(req.params.id, req.body, cb.setupResponseCallback(res));
};
