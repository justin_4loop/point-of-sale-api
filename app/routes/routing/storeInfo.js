'use strict';

var cb = require('./../../utils/callback');
var storeInfoCtrl = require('../../controllers/storeInfoController').StoreInfo;
var storeInfo = new storeInfoCtrl();





exports.saveStore = function onRequest(req, res) {
    //code goes here
    // console.log(req.body);
    storeInfo.saveStore(req.body, cb.setupResponseCallback(res));
};

exports.getStore = function onRequest(req, res) {
    //code goes here
    storeInfo.getStore(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificStore = function onRequest(req, res) {
    //code goes here
    storeInfo.getSpecificStore(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteStore = function onRequest(req, res) {
    //code goes here
    storeInfo.deleteStore(req.params.id, cb.setupResponseCallback(res));
};

exports.updateStore = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/

    if (req.body.isVat == null) {
        req.body.isVat = 0;

    }
    storeInfo.updateStore(req.params.id, req.body, cb.setupResponseCallback(res));



};
