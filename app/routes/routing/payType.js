'use strict';

var cb = require('./../../utils/callback');
var payTypeCtrl = require('../../controllers/payTypeController').PayType;
var payType = new payTypeCtrl();





exports.savePayType = function onRequest(req, res) {
    //code goes here
    payType.savePayType(req.body, cb.setupResponseCallback(res));
};

exports.getPayType = function onRequest(req, res) {
    //code goes here
    payType.getPayType(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificPayType = function onRequest(req, res) {
    //code goes here
    payType.getSpecificPayType(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deletePayType = function onRequest(req, res) {
    //code goes here
    payType.deletePayType(req.params.id, cb.setupResponseCallback(res));
};

exports.updatePayType = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    payType.updatePayType(req.params.id, req.body, cb.setupResponseCallback(res));

};
