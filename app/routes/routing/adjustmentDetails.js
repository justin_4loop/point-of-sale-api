'use strict';

var cb = require('./../../utils/callback');
var adjustmentDetailsCtrl = require('../../controllers/adjustmentDetailsController').AdjustmentDetails;
var adjustmentDetails = new adjustmentDetailsCtrl();





exports.saveAdjustmentDetails = function onRequest(req, res) {
    //code goes here
    adjustmentDetails.saveAdjustmentDetails(req.body, cb.setupResponseCallback(res));
};

exports.getAdjustmentDetails = function onRequest(req, res) {
    //code goes here
    adjustmentDetails.getAdjustmentDetails(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificAdjustmentDetails = function onRequest(req, res) {
    //code goes here
    adjustmentDetails.getSpecificAdjustmentDetails(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteAdjustmentDetails = function onRequest(req, res) {
    //code goes here
    adjustmentDetails.deleteAdjustmentDetails(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAdjustmentDetails = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    adjustmentDetails.updateAdjustmentDetails(req.params.id, req.body, cb.setupResponseCallback(res));

};
