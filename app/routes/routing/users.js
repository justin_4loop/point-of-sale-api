'use strict';

exports.logout = function(req, res) {
    req.logout();
    res.status(200).json({
        msg: 'User successfully logout',
        success: true,
        result: null,
        statusCode: 200
    }).end();
};

exports.login = function(req, res) {
    console.log('req.isAuthenticated(): ', req.isAuthenticated());
    //console.log('user: ', req.user);
    res.status(200).json(req.user);
};
