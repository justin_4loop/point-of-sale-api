'use strict';

var cb = require('./../../utils/callback');

var config = require('../../../config/environment/' + process.env.NODE_ENV);

var SyncCtrl = require('../../controllers/Sync_Controller').SyncData;
var Sync = new SyncCtrl();

/** ***************************  P R O D U C T *************************  **/

exports.getProduct = function onRequest(req, res) {
    Sync.getProduct(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getProduct_Variant = function onRequest(req, res) {
    Sync.getProduct_Variant(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getProductType = function onRequest(req, res) {
    Sync.getProductType(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getAttributeValue = function onRequest(req, res) {
    Sync.getAttributeValue(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getInventory = function onRequest(req, res) {
    Sync.getInventory(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getPriceHistory = function onRequest(req, res) {
    Sync.getPriceHistory(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getAttributes = function onRequest(req, res) {
    Sync.getAttributes(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getAttributeGroup = function onRequest(req, res) {
    Sync.getAttributeGroup(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

exports.getAttributeCategoryMap = function onRequest(req, res) {
    Sync.getAttributeCategoryMap(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
};

/** *************************** E N D  P R O D U C T *************************  **/

/** set status code from Device logs to 1 from 0  AND products logs or employee logs, it depends on modlogs type**/
exports.UpdateProductLogsStat = function onRequest(req, res) {
    Sync.UpdateProductLogsStat(req.body, req.params.store_id, req.params.devicename ,req.params.modlogs ,cb.setupSyncResponseCallback(res));
};


/** *************************** E M P L O Y E E *************************  **/

exports.getStores = function onRequest(req, res) {
    Sync.getStores(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getEmployees = function onRequest(req, res) {
    Sync.getEmployees(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getUserAccounts = function onRequest(req, res) {
    Sync.getUserAccounts(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getUserAccess = function onRequest(req, res) {
    Sync.getUserAccess(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getUserRoles = function onRequest(req, res) {
    Sync.getUserRoles(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getUserPrivs = function onRequest(req, res) {
    Sync.getUserPrivs(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getModuleItems = function onRequest(req, res) {
    Sync.getModuleItems(req.params.devicename, cb.setupSyncResponseCallback(res));
};

exports.getModules = function onRequest(req, res) {
    Sync.getModules(req.params.devicename, cb.setupSyncResponseCallback(res));
};

/** *************************** E N D  E M P L O Y E E *************************  **/





// exports.getProduct = function onRequest(req, res) {
//     Sync.getProduct(req.params.store_id, req.params.devicename ,cb.setupSyncResponseCallback(res));
// };

exports.getProduct_Callback = function onRequest(req, res) {
    res.status(200).json(req.params.devicename);
};

exports.getProductImages = function getProductImages(req, res) {
    res.json(config.api_host_url + "/public/tmp/" + req.params.devicename+ ".zip");
};

exports.getEmployeeInfo = function onRequest(req, res) {
   Sync.getEmployeeInfo(req.params.devicename, cb.setupResponseCallback(res));
};

exports.getModulePriv = function onRequest(req, res) {
    Sync.getModulePriv(cb.setupResponseCallback(res));
};

exports.getBankCredit = function onRequest(req, res) {
    Sync.getBankCredit(req.params.store_id, cb.setupResponseCallback(res));
};