'use strict';

var cb = require('./../../utils/callback');
var fs = require('fs');
var multer = require('multer');
var mkdirp = require('mkdirp');

var uploadsCtrl = require('../../controllers/uploadsController').UploadFile;
var fileUploads = new uploadsCtrl();

exports.uploadAFile = function onRequest(req, res) {
    
    var directory = 'public/uploads';
    if (!fs.existsSync(directory)) { // execute if tmp folder does not exist.
        mkdirp(directory, function(err) {
            if (err) {
                console.log("ERROR! Can't make the directory! \n" + err);
            }
        });
    }

    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, directory);
        },
        filename: function(req, file, cb) {
            cb(null, Math.floor(Date.now() / 1000) + '-' + file.originalname);
        }
    });

    var upload = multer({
        storage: storage
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            return res.status(500).json({
                response: err,
                statusCode: 500
            });
        }
        fileUploads.uploadAFile(req.file, cb.setupResponseCallback(res));
    });

};