'use strict';

var cb = require('./../../utils/callback');

var productTypeCtrl = require('../../controllers/productTypeController').ProductType;
var productType = new productTypeCtrl();
// var multer = require('multer');






exports.saveProductType = function onRequest(req, res) {


    // var storage = multer.diskStorage({
    //     destination: function(req, file, cb) {
    //         console.log("file", file);
    //         cb(null, 'public/uploads/img')
    //     },
    //     filename: function(req, file, cb) {
    //         cb(null, file.originalname);
    //     }
    // })

    // var uploadImg = multer({
    //     storage: storage
    // }).single('ptype_image');
    // uploadImg(req, res, function(err) {

    //     if (err) {
    //         // An error occurred when uploading
    //         console.log(err);
    //         return
    //     }


        if (req.file) {
            // req.body.ptype_image = req.file.destination + '/' + req.file.originalname;
            req.body.ptype_image = req.file.destination + '/' + req.file.filename;
            // console.log(req.body);
            productType.saveProductType(req.body, cb.setupResponseCallback(res));

        } else {

            // console.log(req.body);
            productType.saveProductType(req.body, cb.setupResponseCallback(res));
        }




    // })


};

exports.getProductType = function onRequest(req, res) {
    //code goes here
    productType.getProductType(req.body, cb.setupResponseCallback(res));
};

exports.getSpecificProductType = function onRequest(req, res) {
    //code goes here
    productType.getSpecificProductType(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.deleteProductType = function onRequest(req, res) {
    //code goes here

    productType.deleteProductType(req.params.id, cb.setupResponseCallback(res));
};

exports.updateProductType = function onRequest(req, res) {
    //code goes here

    // var storage = multer.diskStorage({
    //     destination: function(req, file, cb) {
    //         console.log("file", file);
    //         cb(null, 'public/uploads/img')
    //     },
    //     filename: function(req, file, cb) {
    //         cb(null, file.filename);
    //     }
    // })

    // var uploadImg = multer({
    //     storage: storage
    // }).single('ptype_image');
    // uploadImg(req, res, function(err) {

    //     if (err) {
    //         // An error occurred when uploading
    //         console.log(err);
    //         return
    //     }

        if (req.file) {
            // req.body.ptype_image = req.file.destination + '/' + req.file.originalname;
            req.body.ptype_image = req.file.destination + '/' + req.file.filename;

            productType.updateProductType(req.params.id, req.body, cb.setupResponseCallback(res));

        } else {

            productType.updateProductType(req.params.id, req.body, cb.setupResponseCallback(res));
        }


    // })

};
