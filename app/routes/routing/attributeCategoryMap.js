'use strict';

var cb = require('./../../utils/callback');

var attributeCategoryMapCtrl = require('../../controllers/attributeCategoryMapController').AttributeCategoryMap;
var attributeCategoryMap = new attributeCategoryMapCtrl();


exports.saveAttributeCategoryMap = function onRequest(req, res) {

    //code goes here
    attributeCategoryMap.saveAttributeCategoryMap(req.body, cb.setupResponseCallback(res));
};

exports.getAttributeCategoryMap = function onRequest(req, res) {
    //code goes here
    attributeCategoryMap.getAttributeCategoryMap(req.body, cb.setupResponseCallback(res));
};

exports.deleteAttributeCategoryMap = function onRequest(req, res) {
    //code goes here
    attributeCategoryMap.deleteAttributeCategoryMap(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAttributeCategoryMap = function onRequest(req, res) {
    //code goes here
    attributeCategoryMap.updateAttributeCategoryMap(req.params.id, req.body, cb.setupResponseCallback(res));
};
