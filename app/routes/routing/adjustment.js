'use strict';

var cb = require('./../../utils/callback');
var adjustmentCtrl = require('../../controllers/adjustmentController').Adjustment;
var adjustment = new adjustmentCtrl();





exports.saveAdjustment = function onRequest(req, res) {
    //code goes here
    adjustment.saveAdjustment(req.body, cb.setupResponseCallback(res));
};

exports.getAdjustment = function onRequest(req, res) {
    //code goes here
    adjustment.getAdjustment(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificAdjustment = function onRequest(req, res) {
    //code goes here
    adjustment.getSpecificAdjustment(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteAdjustment = function onRequest(req, res) {
    //code goes here
    adjustment.deleteAdjustment(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAdjustment = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    adjustment.updateAdjustment(req.params.id, req.body, cb.setupResponseCallback(res));

};
