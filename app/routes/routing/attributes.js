'use strict';

var cb = require('./../../utils/callback');

var attributesCtrl = require('../../controllers/attributesController').Attributes;
var attributes = new attributesCtrl();
var multer = require('multer');





exports.saveAttributes = function onRequest(req, res) {

    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'public/uploads/img/attributes')
        },
        filename: function(req, file, cb) {
            cb(null, file.originalname);
        }
    })

    var uploadImg = multer({
        storage: storage
    }).array('value_image');
    uploadImg(req, res, function(err) {

        console.log("AttributesRouting", req.body.Attvalue);
        console.log("Request Body", req.body);
        console.log("AttributesRouting File", req.file);
        console.log("AttributesRouting Files", req.files);

        if (err) {
            // An error occurred when uploading
            // console.log(err);
            return
        }else{

            if(req.body.fields.type == 'Attachment'){

                console.log("Attachment Here!");

                req.body.val = req.body.attachment;

            }else if(req.body.fields.type == 'Color Picker'){

                console.log("Color Picker Here!");

                var values = req.body.Attcolor;
        console.log("length", values.length);
        var val = [];
            var temp_name;
            var temp_color;

            for (var index = 0; index < values.length; ++index) {

                temp_name = values[index].name;
                temp_color = values[index].color;
                var obj = {
                    name: temp_name,
                    color: temp_color
                };
                val.push(obj);

            }
            req.body.val = val;

            }else{

                console.log("DropDown and MultiSelect Here!");

                var values = req.body.Attvalue;
        console.log("length", values.length);
        var images = req.file;
        var val = [];
            var temp_image;
            var temp_value;
            var temp_sku;

            for (var index = 0; index < values.length; ++index) {

                if(req.file){
                temp_image = images[index].destination + '/' + images[index].originalname;
                }

                temp_value = values[index].value;
                temp_sku = values[index].sku;
                var obj = {
                    value_image: temp_image,
                    value: temp_value,
                    sku: temp_sku
                };
                val.push(obj);

            }
            req.body.val = val;

            }


            attributes.saveAttributes(req.body, cb.setupResponseCallback(res));
    }


    })



};

exports.getAttributes = function onRequest(req, res) {
    //code goes here
    attributes.getAttributes(req.body, cb.setupResponseCallback(res));
};

exports.getSpecificAttributes = function onRequest(req, res) {
    //code goes here
    console.log("Routing ID", req.params.id);
    attributes.getSpecificAttributes(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.deleteAttributes = function onRequest(req, res) {
    //code goes here
    attributes.deleteAttributes(req.params.id, cb.setupResponseCallback(res));
};

exports.updateAttributes = function onRequest(req, res) {
    //code goes here


    var storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'public/uploads/img/attributes')
        },
        filename: function(req, file, cb) {
            cb(null, file.originalname);
        }
    })

    var uploadImg = multer({
        storage: storage
    }).array('value_image');
    uploadImg(req, res, function(err) {

        console.log("AttributesRouting", req.body.Attvalue);
        console.log("Request Body", req.body);
        console.log("AttributesRouting File", req.file);
        console.log("AttributesRouting Files", req.files);

        if (err) {
            // An error occurred when uploading
            // console.log(err);
            return
        }else{

            if(req.body.fields.type == 'Attachment'){

                console.log("Attachment Here!");

                req.body.val = req.body.attachment;

            }else if(req.body.fields.type == 'Color Picker'){

                console.log("Color Picker Here!");

                var values = req.body.Attcolor;
        console.log("length", values.length);
        var val = [];
            var temp_name;
            var temp_color;

            for (var index = 0; index < values.length; ++index) {

                temp_name = values[index].name;
                temp_color = values[index].color;
                var obj = {
                    name: temp_name,
                    color: temp_color
                };
                val.push(obj);

            }
            req.body.val = val;

            }else{

                console.log("DropDown and MultiSelect Here!");

                var values = req.body.Attvalue;
        console.log("length", values.length);
        var images = req.file;
        var val = [];
            var temp_image;
            var temp_value;
            var temp_sku;

            for (var index = 0; index < values.length; ++index) {

                if(req.file){
                temp_image = images[index].destination + '/' + images[index].originalname;
                }

                temp_value = values[index].value;
                temp_sku = values[index].sku;
                var obj = {
                    value_image: temp_image,
                    value: temp_value,
                    sku: temp_sku
                };
                val.push(obj);

            }
            req.body.val = val;

            }


            // attributes.saveAttributes(req.body, cb.setupResponseCallback(res));

    attributes.updateAttributes(req.params.id, req.body, cb.setupResponseCallback(res));

    }


    })




    // attributes.updateAttributes(req.params.id, req.body, cb.setupResponseCallback(res));
};
