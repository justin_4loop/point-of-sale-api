'use strict';

var cb = require('./../../utils/callback');

var supplierCtrl = require('../../controllers/supplierController').Supplier;
var supplier = new supplierCtrl();




exports.saveSupplier = function onRequest(req, res) {

    //code goes here
    supplier.saveSupplier(req.body, cb.setupResponseCallback(res));
};

exports.getSupplier = function onRequest(req, res) {
    //code goes here
    supplier.getSupplier(req.body, cb.setupResponseCallback(res));
};

exports.deleteSupplier = function onRequest(req, res) {
    //code goes here
    supplier.deleteSupplier(req.params.id, cb.setupResponseCallback(res));
};

exports.updateSupplier = function onRequest(req, res) {
    //code goes here
    supplier.updateSupplier(req.params.id, req.body, cb.setupResponseCallback(res));
};
