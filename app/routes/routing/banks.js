'use strict';

var cb = require('./../../utils/callback');
var banksCtrl = require('../../controllers/banksController').Banks;
var banks = new banksCtrl();





exports.saveBanks = function onRequest(req, res) {
    //code goes here
    banks.saveBanks(req.body, cb.setupResponseCallback(res));
};

exports.getBanks = function onRequest(req, res) {
    //code goes here
    banks.getBanks(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificBanks = function onRequest(req, res) {
    //code goes here
    banks.getSpecificBanks(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteBanks = function onRequest(req, res) {
    //code goes here
    banks.deleteBanks(req.params.id, cb.setupResponseCallback(res));
};

exports.updateBanks = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    banks.updateBanks(req.params.id, req.body, cb.setupResponseCallback(res));

};
