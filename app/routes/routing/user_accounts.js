'use strict';

var cb = require('./../../utils/callback');

var user_accountsCtrl = require('../../controllers/user_accountsController').Users;
var userGroupsCtrl = require('../../controllers/userGroupController').UserGroups;
var userGroupPermissionCtrl = require('../../controllers/userGroupController').userGroupPermission;

var user_accounts = new user_accountsCtrl();
var userGroups = new userGroupsCtrl();
var userGroupPermission = new userGroupPermissionCtrl();

exports.createUser = function onRequest(req, res) {
    user_accounts.createUser(req.body, cb.setupResponseCallback(res));
};

exports.getUser = function onRequest(req, res) {
    user_accounts.getUser(cb.setupResponseCallback(res));
};

exports.getUserById = function onRequest(req, res) {
    user_accounts.getUserById(req.params.id,cb.setupResponseCallback(res));
};

exports.deleteUser = function onRequest(req, res) {
    user_accounts.deleteUser(req.params.id, cb.setupResponseCallback(res));
};

exports.updateUser = function onRequest(req, res) {
    user_accounts.updateUser(req.params.id,req.body, cb.setupResponseCallback(res));
};


// ============ USER GROUPS ============ //

exports.createUserGroup = function onRequest(req, res) {
    userGroups.createUserGroup(req.body, cb.setupResponseCallback(res));
};

exports.getAllUserGroups = function onRequest(req, res) {
    userGroups.getAllUserGroups(cb.setupResponseCallback(res));
};

exports.getUserGroupById = function onRequest(req, res) {
    userGroups.getUserGroupById(req.params.id,cb.setupResponseCallback(res));
};

exports.deleteUserGroup = function onRequest(req, res) {
    userGroups.deleteUserGroup(req.params.id, cb.setupResponseCallback(res));
};

exports.updateUserGroup = function onRequest(req, res) {
    userGroups.updateUserGroup(req.params.id,req.body, cb.setupResponseCallback(res));
};


// ============ USER Permissions ============ //
exports.generateAccessPermission = function onRequest(req, res) {
    userGroupPermission.generateAccessPermission(req.params.roleid, cb.setupResponseCallback(res));
};

exports.setGroupPrivilege = function onRequest(req, res){
    userGroups.setGroupPrivilege(req.params.roleid,req.body,cb.setupResponseCallback(res));
};


