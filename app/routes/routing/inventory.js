'use strict';

var cb = require('./../../utils/callback');

var inventoryCtrl = require('../../controllers/inventoryController').Inventory;
var inventory = new inventoryCtrl();





exports.saveInventory = function onRequest(req, res) {

    //code goes here
    inventory.saveInventory(req.body, cb.setupResponseCallback(res));
};

exports.getInventory = function onRequest(req, res) {
    //code goes here
    inventory.getInventory(req.body, cb.setupResponseCallback(res));
};

exports.deleteInventory = function onRequest(req, res) {
    //code goes here
    inventory.deleteInventory(req.params.id, cb.setupResponseCallback(res));
};

exports.updateInventory = function onRequest(req, res) {
    //code goes here
    inventory.updateInventory(req.params.id, req.body, cb.setupResponseCallback(res));
};
