'use strict';

var cb = require('./../../utils/callback');
var receiveItemsCtrl = require('../../controllers/receiveItemsController').ReceiveItems;
var receiveItems = new receiveItemsCtrl();





exports.saveReceiveItems = function onRequest(req, res) {
    //code goes here
    receiveItems.saveReceiveItems(req.body, cb.setupResponseCallback(res));
};

exports.getReceiveItems = function onRequest(req, res) {
    //code goes here
    receiveItems.getReceiveItems(req.body, cb.setupResponseCallback(res));

};

exports.getSpecificReceiveItems = function onRequest(req, res) {
    //code goes here
    receiveItems.getSpecificReceiveItems(req.params.id, req.body, cb.setupResponseCallback(res));

};

exports.deleteReceiveItems = function onRequest(req, res) {
    //code goes here
    receiveItems.deleteReceiveItems(req.params.id, cb.setupResponseCallback(res));
};

exports.updateReceiveItems = function onRequest(req, res) {
    //code goes here

    /*console.log("req.params.id", req.params.id);
    console.log("req.body", req.body);*/
    receiveItems.updateReceiveItems(req.params.id, req.body, cb.setupResponseCallback(res));

};
