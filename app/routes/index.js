/*jshint camelcase: false */

'use strict';
var multer = require('multer');

// ======================== VALIDATION ============================ //
var validatorUsers = require('../validation/user');
var validatorEmployees = require('../validation/employees');
var validatorUserAcc = require('../validation/useracc');

var validatorProductType = require('../validation/productType');
var validatorAttributes = require('../validation/attributes');
var validatorCategories = require('../validation/categories');
var validatorStore = require('../validation/store');
var validatorSupplier = require('../validation/supplier');
var validatorPriceHistory = require('../validation/priceHistory');
var validatorVariant = require('../validation/variant');
var validatorInventory = require('../validation/inventory');
var validatorAttributeGrp = require('../validation/attributeGrp');
var validatorAttributeValue = require('../validation/attributeValue');
var validatorProductCategory = require('../validation/productCategory');
var validatorAttributeCategoryMap = require('../validation/attributeCategoryMap');
var validatorPurchaseOrder = require('../validation/purchaseOrder');
var validatorReceivePurchaseOrder = require('../validation/receivePurchaseOrder');
var validatorReceiveItems = require('../validation/receiveItems');
var validatorAdjustmentType = require('../validation/adjustmentType');
var validatorAdjustment = require('../validation/adjustment');
var validatorBanks = require('../validation/banks');
var validatorCardType = require('../validation/cardType');
var validatorCreditDetails = require('../validation/creditDetails');
var validatorAdjustmentDetails = require('../validation/adjustmentDetails');
var validatorPayType = require('../validation/payType');
var validatorPayment = require('../validation/payment');
var validatorProduct = require('../validation/products');


// ======================== ROUTING ============================ //
var users = require('./routing/users');
var user_accounts = require('./routing/user_accounts');
var employees = require('./routing/employees');
var modules = require('./routing/modules');
var products = require('./routing/products');
var storeInfo = require('./routing/storeInfo');
var supplier = require('./routing/supplier');
var categories = require('./routing/categories');
var attributes = require('./routing/attributes');
var productType = require('./routing/productType');
var priceHistory = require('./routing/priceHistory');
var variant = require('./routing/variant');
var inventory = require('./routing/inventory');
var attributeGrp = require('./routing/attributeGrp');
var attributeValue = require('./routing/attributeValue');
var productCategory = require('./routing/productCategory');
var attributeCategoryMap = require('./routing/attributeCategoryMap');
var fileUploads = require('./routing/fileUploads');
var purchaseOrder = require('./routing/purchaseOrder');
var receivePurchaseOrder = require('./routing/receivePurchaseOrder');
var receiveItems = require('./routing/receiveItems');
var adjustmentType = require('./routing/adjustmentType');
var adjustment = require('./routing/adjustment');
var banks = require('./routing/banks');
var cardType = require('./routing/cardType');
var creditDetails = require('./routing/creditDetails');
var adjustmentDetails = require('./routing/adjustmentDetails');
var payType = require('./routing/payType');
var payment = require('./routing/payment');

module.exports = function(app, passport, config, middleware) {

    app.route('/')
        .get(function onRequest(req, res) {
            res.render('index');
        });

    // ======================== AUTHENTICATION ============================ //
    app.route(config.api_version + '/login')
        .post(validatorUsers.validateLogin, passport.authenticate('user'), users.login);

    app.route(config.api_version + '/logout')
        .get(users.logout);

    // ======================== Modules ============================ //
    app.route(config.api_version + '/modules')
        .get(modules.getAllModules);
    app.route(config.api_version + '/modules/permissions')
        .get(modules.getAllModulesPermissions);


    // ======================== Modules Items ============================ //
    app.route(config.api_version + '/module_item')
        .get(modules.getAllModuleItems);

    // ======================== Employees ============================ //

    app.route(config.api_version + '/employees')
        .post(validatorEmployees.validateEmployees, employees.createEmployee)
        .get(middleware.isLoggedIn, middleware.isTokenActive, employees.getEmployee);

    app.route(config.api_version + '/employeeswithoutaccount')
        .get(middleware.isLoggedIn, middleware.isTokenActive, employees.getEmployeeWithoutAccount);

    app.route(config.api_version + '/employees/:id')
        .get(middleware.isLoggedIn, employees.getEmployeeById)
        .delete(middleware.isLoggedIn, employees.deleteEmployee)
        .put(validatorEmployees.validateEmployees, middleware.isLoggedIn, employees.updateEmployee);

    // ======================== User Accounts ============================ //

    app.route(config.api_version + '/useraccounts')
        /*.post(validatorUserAcc.validateUserAcc,user_accounts.createUser)
        .get(middleware.isLoggedIn, middleware.isTokenActive, user_accounts.getUser);*/
        .post(user_accounts.createUser)
        .get(user_accounts.getUser);

    app.route(config.api_version + '/useraccounts/:id')
        /*.get(middleware.isLoggedIn,user_accounts.getUserById)
        .delete(middleware.isLoggedIn, user_accounts.deleteUser)
        .put(validatorUserAcc.validateUserAcc,middleware.isLoggedIn,user_accounts.updateUser);*/
        .get(user_accounts.getUserById)
        .delete(user_accounts.deleteUser)
        .put(user_accounts.updateUser);

    app.route(config.api_version + '/useraccounts/:roleid/permissions')
        .get(user_accounts.generateAccessPermission)
        .put(user_accounts.setGroupPrivilege);

    // ======================== User Groups ============================ //

    app.route(config.api_version + '/usergroups')
        .post(user_accounts.createUserGroup)
        .get(user_accounts.getAllUserGroups);

    app.route(config.api_version + '/usergroups/:id')
        .get(user_accounts.getUserGroupById)
        .delete(user_accounts.deleteUserGroup)
        .put(user_accounts.updateUserGroup);

    // ======================== Product ============================ //

    app.route(config.api_version + '/product')
        .post(validatorProduct.validateProducts, products.saveProduct)
        .get(products.getProducts);

    app.route(config.api_version + '/product/:id')
        .get(products.getProduct)
        .delete(products.deleteProduct)
        .put(validatorProduct.validateProducts, products.updateProduct);


    // ======================== Product Archive ============================ //

    app.route(config.api_version + '/products/active')
        .get(products.getActive);

    app.route(config.api_version + '/products/archive')
        .get(products.getArchive);

    app.route(config.api_version + '/products/:id/archive')
        .put(products.archiveProduct);

    // ======================== Product Publish ============================ //
    app.route(config.api_version + '/products/publish')
        .get(products.getPublish);

    app.route(config.api_version + '/products/unpublish')
        .get(products.getUnpublish);

    app.route(config.api_version + '/products/:id/publish')
        .put(products.publishProduct);



    // ======================== Product Search by Name ============================ //

    app.route(config.api_version + '/searchProduct/:productName')
        .get(products.searchProduct);

    // ======================== Product FIlter by Category ============================ //

    app.route(config.api_version + '/productCategoryFilter/:id')
        .get(products.productCategoryFilter);




    // ======================== Product Type============================ //

    app.route(config.api_version + '/productType')
        .post(multer({dest: 'public/uploads/img/productType'}).single('ptype_image'), validatorProductType.validateProductType, productType.saveProductType)
        // .post(productType.saveProductType)
        .get(productType.getProductType);

    app.route(config.api_version + '/producttype/:id')
        .get(productType.getSpecificProductType)
        .delete(productType.deleteProductType)
        .put(multer({dest: 'public/uploads/img/productType'}).single('ptype_image'), validatorProductType.validateProductType, productType.updateProductType);



    // ======================== Attributes ============================ //

    app.route(config.api_version + '/attributes')
        // .post(validatorAttributes.validateAttributes, attributes.saveAttributes)
        .post(attributes.saveAttributes)
        .get(attributes.getAttributes);

    app.route(config.api_version + '/attributes/:id')
        .delete(attributes.deleteAttributes)
        .put(attributes.updateAttributes)
        .get(attributes.getSpecificAttributes);



    // ======================== Store ============================ //

    app.route(config.api_version + '/store')
        .post(validatorStore.validateStore, storeInfo.saveStore)
        .get(storeInfo.getStore);

    app.route(config.api_version + '/store/:id')
        .get(storeInfo.getSpecificStore)
        .delete(storeInfo.deleteStore)
        .put(storeInfo.updateStore);


    // ======================== Supplier ============================ //

    app.route(config.api_version + '/supplier')
        .post(validatorSupplier.validateSupplier, supplier.saveSupplier)
        .get(supplier.getSupplier);

    app.route(config.api_version + '/supplier/:id')
        .delete(supplier.deleteSupplier)
        .put(supplier.updateSupplier);


    // ======================== Categories ============================ //

    app.route(config.api_version + '/categories')
        .post(multer({dest: 'public/uploads/img/categories'}).single('c_image'), validatorCategories.validateCategories, categories.saveCategories)
        .get(categories.getCategories);
    // .post(categories.saveCategories)
    

    app.route(config.api_version + '/categories/:id')
        .delete(categories.deleteCategories)
        .put(categories.updateCategories)
        .get(categories.getChildCategories);


    // Route for Update Category
    app.route(config.api_version + '/category/:id')
        .get(categories.getSpecificCategory);

    // ======================== Price History ============================ //

    app.route(config.api_version + '/storeowner/:id')
        .get(priceHistory.getStoreOwner);

    /*app.route(config.api_version + '/storeOwnerBoolean/:id')
        .get(priceHistory.getstoreOwnerBoolean);*/

    app.route(config.api_version + '/pricehistory')
        .get(priceHistory.getPriceHistory);


    app.route(config.api_version + '/pricehistory/:id')
        .put(priceHistory.savePriceHistory)
        .delete(priceHistory.deletePriceHistory)
        .get(priceHistory.getSpecificPriceHistory);

    app.route(config.api_version + '/pricehistory/:id/detail')
        .put(priceHistory.updatePriceHistory);

    // ======================== Product Variants ============================ //

    app.route(config.api_version + '/variant')
        .post(validatorVariant.validateVariant, variant.saveVariant)
        .get(variant.getVariant);

    app.route(config.api_version + '/variant/:id')
        .delete(variant.deleteVariant)
        .put(variant.updateVariant);





    // ======================== Inventory ============================ //

    app.route(config.api_version + '/inventory')
        .post(validatorInventory.validateInventory, inventory.saveInventory)
        .get(inventory.getInventory);

    app.route(config.api_version + '/inventory/:id')
        .delete(inventory.deleteInventory)
        .put(inventory.updateInventory);


    // ======================== Attribute Group ============================ //

    app.route(config.api_version + '/attributeGrp')
        .post(validatorAttributeGrp.validateAttributeGrp, attributeGrp.saveAttributeGrp)
        .get(attributeGrp.getAttributeGrp);

    app.route(config.api_version + '/attributeGrp/:id')
        .delete(attributeGrp.deleteAttributeGrp)
        .put(attributeGrp.updateAttributeGrp);

    // ======================== Attribute Group ============================ //

    app.route(config.api_version + '/attributeValue')
        .post(validatorAttributeValue.validateAttributeValue, attributeValue.saveAttributeValue)
        .get(attributeValue.getAttributeValue);

    app.route(config.api_version + '/attributeValue/:id')
        .delete(attributeValue.deleteAttributeValue)
        .put(attributeValue.updateAttributeValue);

    // ======================== Product Category ============================ //

    app.route(config.api_version + '/productCategory')
        .post(validatorProductCategory.validateProductCategory, productCategory.saveProductCategory)
        .get(productCategory.getProductCategory);

    app.route(config.api_version + '/productCategory/:id')
        .delete(productCategory.deleteProductCategory)
        .put(productCategory.updateProductCategory);


    // ======================== Product Category ============================ //

    app.route(config.api_version + '/attributeCategoryMap')
        .post(validatorAttributeCategoryMap.validateAttributeCategoryMap, attributeCategoryMap.saveAttributeCategoryMap)
        .get(attributeCategoryMap.getAttributeCategoryMap);

    app.route(config.api_version + '/attributeCategoryMap/:id')
        .delete(attributeCategoryMap.deleteAttributeCategoryMap)
        .put(attributeCategoryMap.updateAttributeCategoryMap);

    // ======================== uploads ============================ //

    app.route(config.api_version + '/fileUpload')
        .post(fileUploads.uploadAFile);


    // ======================== Purchase Order============================ //

    app.route(config.api_version + '/purchaseOrder')
        .post(validatorPurchaseOrder.validatePurchaseOrder, purchaseOrder.savePurchaseOrder)
        .get(purchaseOrder.getPurchaseOrder);

    app.route(config.api_version + '/purchaseOrder/:id')
        .delete(purchaseOrder.deletePurchaseOrder)
        .put(purchaseOrder.updatePurchaseOrder)
        .get(purchaseOrder.getSpecificPurchaseOrder);

    // ======================== Receive Purchase Order ============================ //

    app.route(config.api_version + '/receivePurchaseOrder')
        .post(validatorReceivePurchaseOrder.validateReceivePurchaseOrder, receivePurchaseOrder.saveReceivePurchaseOrder)
        .get(receivePurchaseOrder.getReceivePurchaseOrder);

    app.route(config.api_version + '/receivePurchaseOrder/:id')
        .delete(receivePurchaseOrder.deleteReceivePurchaseOrder)
        .put(receivePurchaseOrder.updateReceivePurchaseOrder)
        .get(receivePurchaseOrder.getSpecificReceivePurchaseOrder);

    // ======================== Receive Items ============================ //

    app.route(config.api_version + '/receiveItems')
        .post(validatorReceiveItems.validateReceiveItems, receiveItems.saveReceiveItems)
        .get(receiveItems.getReceiveItems);

    app.route(config.api_version + '/receiveItems/:id')
        .delete(receiveItems.deleteReceiveItems)
        .put(receiveItems.updateReceiveItems)
        .get(receiveItems.getSpecificReceiveItems);

    // ======================== Adjustment Type ============================ //

    app.route(config.api_version + '/adjustmentType')
        .post(validatorAdjustmentType.validateAdjustmentType, adjustmentType.saveAdjustmentType)
        .get(adjustmentType.getAdjustmentType);

    app.route(config.api_version + '/adjustmentType/:id')
        .delete(adjustmentType.deleteAdjustmentType)
        .put(adjustmentType.updateAdjustmentType)
        .get(adjustmentType.getSpecificAdjustmentType);

    // ======================== Adjustment============================ //

    app.route(config.api_version + '/adjustment')
        .post(validatorAdjustment.validateAdjustment, adjustment.saveAdjustment)
        .get(adjustment.getAdjustment);

    app.route(config.api_version + '/adjustment/:id')
        .delete(adjustment.deleteAdjustment)
        .put(adjustment.updateAdjustment)
        .get(adjustment.getSpecificAdjustment);

    // ======================== Banks ============================ //

    app.route(config.api_version + '/banks')
        .post(validatorBanks.validateBanks, banks.saveBanks)
        .get(banks.getBanks);

    app.route(config.api_version + '/banks/:id')
        .delete(banks.deleteBanks)
        .put(banks.updateBanks)
        .get(banks.getSpecificBanks);

    // ======================== Card Type ============================ //

    app.route(config.api_version + '/cardType')
        .post(validatorCardType.validateCardType, cardType.saveCardType)
        .get(cardType.getCardType);

    app.route(config.api_version + '/cardType/:id')
        .delete(cardType.deleteCardType)
        .put(cardType.updateCardType)
        .get(cardType.getSpecificCardType);

    // ======================== Credit Details ============================ //

    app.route(config.api_version + '/creditDetails')
        .post(validatorCreditDetails.validateCreditDetails, creditDetails.saveCreditDetails)
        .get(creditDetails.getCreditDetails);

    app.route(config.api_version + '/creditDetails/:id')
        .delete(creditDetails.deleteCreditDetails)
        .put(creditDetails.updateCreditDetails)
        .get(creditDetails.getSpecificCreditDetails);

    // ======================== Adjustment Details ============================ //

    app.route(config.api_version + '/adjustmentDetails')
        .post(validatorAdjustmentDetails.validateAdjustmentDetails, adjustmentDetails.saveAdjustmentDetails)
        .get(adjustmentDetails.getAdjustmentDetails);

    app.route(config.api_version + '/adjustmentDetails/:id')
        .delete(adjustmentDetails.deleteAdjustmentDetails)
        .put(adjustmentDetails.updateAdjustmentDetails)
        .get(adjustmentDetails.getSpecificAdjustmentDetails);

    // ======================== Pay Type ============================ //

    app.route(config.api_version + '/payType')
        .post(validatorPayType.validatePayType, payType.savePayType)
        .get(payType.getPayType);

    app.route(config.api_version + '/payType/:id')
        .delete(payType.deletePayType)
        .put(payType.updatePayType)
        .get(payType.getSpecificPayType);

    // ======================== Payment ============================ //

    app.route(config.api_version + '/payment')
        .post(validatorPayment.validatePayment, payment.savePayment)
        .get(payment.getPayment);

    app.route(config.api_version + '/payment/:id')
        .delete(payment.deletePayment)
        .put(payment.updatePayment)
        .get(payment.getSpecificPayment);

};
