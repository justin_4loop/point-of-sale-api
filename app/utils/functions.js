'use strict';

var IntToBoolean = function(intValue) {
    return intValue == 1 ? true : false;
};

var BooleanToInt = function(boolean) {
    return boolean === true ? 1 : 0;
};


exports.IntToBoolean = IntToBoolean;
exports.BooleanToInt = BooleanToInt;