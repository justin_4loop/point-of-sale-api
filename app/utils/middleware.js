'use strict';

var env = process.env.NODE_ENV;
var config = require('../../config/environment/' + env);

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var jwt = require('jsonwebtoken');


var isLoggedIn = function(req, res, next) {
    // if user is authenticated in the session, carry on
    console.log('at isLoggedIn function');
    /*if (req.headers.authorization) {
        return next();
    } else {
        res.sendStatus(401);
    }*/
    return next();
};

var isTokenActive = function(req, res, next) {
    /*var token = (req.headers.authorization).substring(7);
    var decodedToken = jwt.decode(token, {
        complete: true
    });
    jwt.verify(token, config.token_secret, function(err) {
        if (!err) {
            return next();
        } else {
            var user = {};
            user.uc_id = decodedToken.payload.uc_id;
            user.username = decodedToken.payload.username;
            user.password = decodedToken.payload.password;
            user.valid_until = decodedToken.payload.valid_until;
            user.emp_id = decodedToken.payload.emp_id;
            user.isStatus = decodedToken.payload.isStatus;

            db.query('SELECT * FROM token_sessions WHERE session_token = \'' + token + '\'', function(err1, data) {
                if (parseInt(data[0].session_status) === 1 ? true : false) {
                    jwt.sign(user, config.token_secret, {
                        expiresIn: 3600
                    }, function(newToken) {
                        req.headers.authorization = 'Bearer '+newToken;
                        db.query('update token_sessions set session_status = 0 where session_token =\''+token+'\' ', function(err){
                                if(!err){
                                    db.query('INSERT INTO token_sessions(session_token,session_status,session_time) VALUES (\'' + newToken + '\',1,now())',function(err){
                                        if(!err){
                                            return next();
                                        } else {
                                            res.sendStatus('current session token not inserted to db..');
                                        }
                                    });
                                } else {
                                    res.sendStatus('previous session token on db not updated to 0..');
                                }
                            });
                    });
                } else {
                    return res.sendStatus(403);
                }
            });
        }
    });*/
    return next();
};


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept, Cache-Control');
    // next();
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
};

var UserRights = {
    AllowRead: 1,
    AllowWrite: 2,
    AllowDelete: 3,
    AllowPrint: 4,
    AllowExport: 5
};

var getUserPrivilege = function(UserGroupID, pMenuName, pRights, next) {

    //TODO: IF SUPER USER : getUserPrivilege = true;

    var strSQL = 'SELECT GP.role_id,MO.module_id,MO.`name`,ME.item_description,GP.`Read` AS DefaultRead,GP.`Write` AS DefaultWrite, GP.`Delete` AS DefaultDelete, GP.`Print` AS DefaultPrint,ME.inactive,ME.mf_id FROM module_item ME LEFT JOIN modules MO ON ME.module_id = MO.module_id LEFT JOIN user_privs GP ON GP.module_item_id = ME.mf_id WHERE GP.role_id=\'' + UserGroupID + '\' AND ME.item_name=\'' + pMenuName + '\' ORDER BY GP.role_id,MO.module_id;';
    db.query(str, function(err, response) {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        }

        switch (pRights) {
            case UserRights.AllowRead:
                next(null, {
                    result: {
                        getUserPrivilege : true,
                        AllowRead: true
                    },
                    msg: '',
                    success: true
                })
                break;
            case UserRights.AllowWrite:
                next(null, {
                    result: {
                        getUserPrivilege : true,
                        AllowWrite: true
                    },
                    msg: '',
                    success: true
                })
                break;
            case UserRights.AllowDelete:
                next(null, {
                    result: {
                        getUserPrivilege : true,
                        AllowDelete: true
                    },
                    msg: '',
                    success: true
                })
                break;
            case UserRights.AllowPrint:
                next(null, {
                    result: {
                        getUserPrivilege : true,
                        AllowPrint: true
                    },
                    msg: '',
                    success: true
                })
                break;
        }
    });

};




exports.isLoggedIn = isLoggedIn;
exports.isTokenActive = isTokenActive;
exports.allowCrossDomain = allowCrossDomain;
exports.getUserPrivilege = getUserPrivilege;
exports.UserRights = UserRights;