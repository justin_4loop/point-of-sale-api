'use strict';

exports.validateAdjustmentDetails = function(req, res, next) {
    req.checkBody('adjustment_id', 'Please provide Adjustment Name').notEmpty();
    req.checkBody('pay_id', 'Please provide Payment').notEmpty();
    req.checkBody('unit_id', 'Please provide the Unit').notEmpty();
    req.checkBody('amount', 'Please provide the Amount').notEmpty();
    req.checkBody('quantity', 'Please provide the uantity').notEmpty();



    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};