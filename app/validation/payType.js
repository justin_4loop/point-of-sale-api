'use strict';

exports.validatePayType = function(req, res, next) {
    req.checkBody('name', 'Please provide Pay Type Name').notEmpty();
    req.checkBody('description', 'Please provide Pay Type Description').notEmpty();
    
    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};