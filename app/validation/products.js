'use strict';

exports.validateProducts = function(req, res, next) {

    req.checkBody('item_name', 'Please enter product name').notEmpty();
    req.checkBody('item_description', 'Please enter product description').notEmpty();
    req.checkBody('product_code', 'Please enter product code').notEmpty();
    req.checkBody('ptype_id', 'Please provide product type').notEmpty();
    req.checkBody('brand', 'Please enter product brand').notEmpty();
    req.checkBody('sell_price', 'Please enter selling price').notEmpty();
    req.checkBody('start_date', 'Please provide product start date').notEmpty();
    req.checkBody('end_date', 'Please provide product end date').notEmpty(); 
    

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(200).send({
            response: {
                result: errors,
                success: false,
                msg: ''
            },
            statusCode: 200,
        });
    } else {
        next();
    }
};
