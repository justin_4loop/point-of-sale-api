'use strict';

exports.validatePriceHistory = function(req, res, next) {
    req.checkBody('store_id', 'Please provide Store ID').notEmpty();
    req.checkBody('name', 'Please provide Store Name').notEmpty();
    req.checkBody('owner_name', 'Please provide Store Owner Name').notEmpty();
    req.checkBody('address', 'Please provide Owner Address').notEmpty();
    req.checkBody('ph_id', 'Please provide Price History ID').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};