'use strict';

exports.validateEmployees = function(req, res, next) {
    req.checkBody('lastname', 'Please provide your lastname').notEmpty();
    req.checkBody('firstname', 'Please provide your firstname').notEmpty();
    req.checkBody('isActive', 'Please provide isActive').notEmpty();
    req.checkBody('store_id', 'Please provide your store_id').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(200).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 200
        });
    } else {
        next();
    }
};