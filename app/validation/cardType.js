'use strict';

exports.validateCardType = function(req, res, next) {
    req.checkBody('name', 'Please provide Card Type Name').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};