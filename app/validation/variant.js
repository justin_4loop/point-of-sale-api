'use strict';

exports.validateVariant = function(req, res, next) {
    req.checkBody('attributeValue_id', 'Please provide Attribute Value').notEmpty();
    req.checkBody('att_id', 'Please provide Attribute Name').notEmpty();
    req.checkBody('prod_id', 'Please provide Product Name').notEmpty();
    req.checkBody('ph_id', 'Please provide Price History ID').notEmpty();
    req.checkBody('sku', 'Please provide SKU').notEmpty();
    req.checkBody('product_imgPath', 'Please provide Variant Image').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};