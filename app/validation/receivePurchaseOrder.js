'use strict';

exports.validateReceivePurchaseOrder = function(req, res, next) {
    req.checkBody('po_id', 'Please provide Purchase Order Name').notEmpty();
    req.checkBody('receive_number', 'Please provide Receive Number').notEmpty();
    req.checkBody('receive_date', 'Please provide Date').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};