'use strict';

exports.validateCreditDetails = function(req, res, next) {
    req.checkBody('bank_id', 'Please provide Bank Name').notEmpty();
    req.checkBody('card_type_id', 'Please provide Card Type Name').notEmpty();
    req.checkBody('name_on_card', 'Please provide your Name on Card').notEmpty();
    req.checkBody('exp_date', 'Please provide Expiration Date').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};