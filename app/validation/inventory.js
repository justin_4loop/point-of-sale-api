'use strict';

exports.validateInventory = function(req, res, next) {
    req.checkBody('fk_store_id', 'Please provide Store Name').notEmpty();
    req.checkBody('fk_p_id', 'Please provide Product Name').notEmpty();
    req.checkBody('status', 'Please provide Status').notEmpty();
    req.checkBody('stock_onhand', 'Please provide Stock on Hand').notEmpty();
    req.checkBody('reorder_level', 'Please provide Reorder Level').notEmpty();
    req.checkBody('outOfStock_reason', 'Please provide Out of Stock Reason').notEmpty().isEmail();
    req.checkBody('reStock_date', 'Please provide Restock Date').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};