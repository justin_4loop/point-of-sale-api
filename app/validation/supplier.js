'use strict';

exports.validateSupplier = function(req, res, next) {
    req.checkBody('name', 'Please provide Supplier Name').notEmpty();
    req.checkBody('contact_person', 'Please provide Supplier Contact Person').notEmpty();
    req.checkBody('contact_number', 'Please provide Supplier Contacct Number').notEmpty();
    req.checkBody('address', 'Please provide Supplier Address').notEmpty();
    req.checkBody('isActive', 'Please provide Supplier Status').notEmpty().isInt();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};