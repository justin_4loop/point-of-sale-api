'use strict';

exports.validateAttributeGrp = function(req, res, next) {
    req.checkBody('name', 'Please provide Attribute Group Name').notEmpty();
    req.checkBody('label', 'Please provide Attribute Group Label').notEmpty();
    req.checkBody('description', 'Please provide Attribute Group Description').notEmpty();
    req.checkBody('sort_order', 'Please provide Sort Order').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};