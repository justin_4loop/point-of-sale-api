'use strict';

exports.validateAttributeValue = function(req, res, next) {
    req.checkBody('product_id', 'Please provide Product Name').notEmpty();
    req.checkBody('attribute_id', 'Please provide Attibute Name').notEmpty();
    req.checkBody('priceHistory_id', 'Please provide Price History Name').notEmpty();
    req.checkBody('value', 'Please provide Value').notEmpty();
    req.checkBody('sku', 'Please provide SKU').notEmpty()

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};