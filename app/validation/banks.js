'use strict';

exports.validateBanks = function(req, res, next) {
    req.checkBody('name', 'Please provide Bank Name').notEmpty();
    req.checkBody('address', 'Please provide Bank Address').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};