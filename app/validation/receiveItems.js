'use strict';

exports.validateReceiveItems = function(req, res, next) {
    req.checkBody('r_id', 'Please provide Receive Purchase Order Name').notEmpty();
    req.checkBody('unit_id', 'Please provide Variation').notEmpty();
    req.checkBody('quantity', 'Please provide Quantity').notEmpty();
    req.checkBody('received_by', 'Please provide Employee Name').notEmpty();



    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};