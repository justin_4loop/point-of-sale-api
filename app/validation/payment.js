'use strict';

exports.validatePayment = function(req, res, next) {
    req.checkBody('pay_type_id', 'Please provide Pay Type').notEmpty();
    req.checkBody('credit_details', 'Please provide Credit Details').notEmpty();
    req.checkBody('amount_tendered', 'Please provide Amount Tendered').notEmpty();
    req.checkBody('paid_amount', 'Please provide Paid Amount').notEmpty();
    req.checkBody('or_number', 'Please provide OR Number').notEmpty();
    req.checkBody('attended_by', 'Please provide Employee Name').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};