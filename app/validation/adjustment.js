'use strict';

exports.validateAdjustment = function(req, res, next) {
    req.checkBody('adj_number', 'Please provide Adjustment Number').notEmpty();
    req.checkBody('adj_type', 'Please provide Adjustment Type').notEmpty();
    req.checkBody('date', 'Please provide Date').notEmpty();
    req.checkBody('adjusted_by', 'Please provide Adjusted By').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};