'use strict';

exports.validatePurchaseOrder = function(req, res, next) {
    req.checkBody('po_number', 'Please provide Purchase Order Number').notEmpty();
    req.checkBody('po_date', 'Please provide Purchase Order Date').notEmpty();
    req.checkBody('po_status', 'Please provide Purchase Order Staus').notEmpty();
    req.checkBody('created_by', 'Please provide Employee Name').notEmpty();
    req.checkBody('supplier_id', 'Please provide your Supplier Name').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};