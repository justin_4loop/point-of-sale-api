'use strict';

exports.validateProductType = function(req, res, next) {
    // console.log("validator", req.body); 
    req.checkBody('name', 'Please provide Product Type Name').notEmpty();
    req.checkBody('label', 'Please provide Product Type Label').notEmpty();
    req.checkBody('description', 'Please provide Product Type Description').notEmpty();
    // req.checkBody('ptype_image', 'Please provide Product Type Image').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};