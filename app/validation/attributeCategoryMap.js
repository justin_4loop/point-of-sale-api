'use strict';

exports.validateAttributeCategoryMap = function(req, res, next) {
    req.checkBody('fk_ptype_id', 'Please provide fk_ptype_id').notEmpty();
    req.checkBody('fk_att_id', 'Please provide fk_att_id').notEmpty();
    req.checkBody('fk_attgrp_id', 'Please provide fk_attgrp_id').notEmpty();



    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};