'use strict';

exports.validateProductCategory = function(req, res, next) {
    req.checkBody('fk_product_id', 'Please provide Product Name').notEmpty();
    req.checkBody('fk_category_id', 'Please provide Category Name').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};