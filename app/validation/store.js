'use strict';

exports.validateStore = function(req, res, next) {
    console.log("Validator", req.body);
    req.checkBody('name', 'Please provide Store Name').notEmpty();
    req.checkBody('address', 'Please provide Store Address').notEmpty();
    req.checkBody('tin', 'Please provide Store TIN').notEmpty()
    req.checkBody('owner_name', 'Please provide Store Owner Name').notEmpty();
    req.checkBody('telno', 'Please provide your Store Contact Number').notEmpty()
    req.checkBody('email_address', 'Please provide Store Email Address').notEmpty().isEmail();

    var errors = req.validationErrors();
    console.log("ERRORS", errors);

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};