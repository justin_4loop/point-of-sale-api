'use strict';

exports.validateUserAcc = function(req, res, next) {
    req.checkBody('username', 'Please provide your Username').notEmpty();
    req.checkBody('password', 'Please provide your Password').notEmpty();
    req.checkBody('valid_until', 'Please provide date').notEmpty().isDate();
    req.checkBody('emp_id', 'Please provide your emp_id').notEmpty().isNumber;
    req.checkBody('role_id', 'Please provide role_id').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(200).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 200
        });
    } else {
        next();
    }
};