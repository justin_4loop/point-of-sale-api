'use strict';

exports.validateCategories = function(req, res, next) {
    console.log("Validator body", req.body);
    console.log("Validator file", req.file);
    req.checkBody('name', 'Please provide Category Name').notEmpty();
    req.checkBody('label', 'Please provide Category Label').notEmpty();
    req.checkBody('description', 'Please provide Category Description').notEmpty();
    // req.assert('c_image', 'Please provide Store Image').notEmpty();
    // req.checkBody('telno', 'Please provide your Store Contact Number').notEmpty()
    // req.checkBody('email_address', 'Please provide Store Email Address').notEmpty().isEmail();

    var errors = req.validationErrors();
    console.log("Error", errors);

    if (errors) {
        console.log('yyyy');
        res.status(400).send({
            response: errors,
            statusCode: 400,
            success: false
        });
    } else {
        next();
    }
};