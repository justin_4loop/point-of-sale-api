'use strict';
var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);
var functions = require('../../app/utils/functions');

exports.saveAttributes = function saveAttributes(data, next) {
    //code goes here.
    // console.log("AttributeDao", data);

    var connection = mysql.createConnection({
        host: config.db_host,
        user: config.db_user,
        password: config.db_password,
        database: config.db_name,
        multipleStatements: true
    });


            //attribute
            var attributesObj = {
                name: data.name,
                label: data.label,
                type: data.type,
                is_visible: data.is_visible,
                is_required: data.is_required,
                is_variation: data.is_variation,
                is_custumizable: data.is_custumizable,
                is_unique: data.is_unique
            };

            //attribute value
            var val = data.val

            //attribute category map
            var fk_ptype_id = data.productType_id

            console.log("attibutesObj", attributesObj);
            console.log("val", val);
            console.log("fk_ptype_id", fk_ptype_id);




            connection.beginTransaction(function(err) {
        if (err) {
            next(err, {
                msg: 'fail connecting to server',
                statuscode: '500',
                success: false,
                errmsg: err
            })
        } else {

    async.waterfall([
        function(callback) {
            console.log("attributesObj", attributesObj);
            var str = mysql.format('INSERT into attributes SET ?', attributesObj);
            connection.query(str, function(err0, resp0) {
                        if (err0) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attributes');
                                callback(err0, null);
                            });

                        }
                        callback(null, resp0);
                    });
        },
        function(resp0, callback) {
            console.log("attibutes resp", resp0);
            var attribute_id = resp0.insertId;

            var data = [];
            console.log("attributesObj.type", attributesObj.type);
            if(attributesObj.type == 'Color Picker'){

                val.forEach(function(obj) {

                var temp = [];
                temp.push(attribute_id);
                temp.push(124);
                temp.push(obj.name);
                temp.push(obj.color);

                data.push(temp);
            });

                console.log("attribute_value Object", data);
            var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, name, color) Values ?', [data]);

            }else if(attributesObj.type == 'Attachment'){

                  console.log("attribute_value Attachment Object", val);

                  var attachmentObj = {
                    attribute_id: attribute_id,
                    product_id: 124,
                    is_single: val.is_single,
                    is_image: val.attchtype.attachment_Image,
                    is_video: val.attchtype.attachment_Video,
                    is_audio: val.attchtype.attachment_Audio,
                    is_document: val.attchtype.attachment_Document
                  };

            var str = mysql.format('INSERT into attribute_value SET ?', attachmentObj);


            

            }else{

                val.forEach(function(obj) {

                var temp = [];
                temp.push(attribute_id);
                temp.push(124);
                temp.push(obj.value);
                temp.push(obj.sku);
                temp.push(obj.value_image);

                data.push(temp);
            });

                console.log("attribute_value Object", data);
            var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);

            }
            


            // console.log("attribute_value Object", data);
            // var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);
            connection.query(str, function(err1, resp1) {
                        if (err1) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attribute_value');
                                callback(err0, null);
                            });

                        }
                        callback(null, resp1, attribute_id);
                    });
        },
        // function(attribute_ID, attributeValue_ID, val, callback) {

        //     var data = [];
        //     val.forEach(function(obj) {
        //         var temp = [];
        //         temp.push(attributeValue_ID);
        //         temp.push(attribute_ID);
        //         temp.push(124);
        //         temp.push(obj.sku);
        //         temp.push(obj.image);

        //         data.push(temp);


        //     });

        //     var str = mysql.format('INSERT into product_variant (attributeValue_id, att_id, prod_id,sku, product_imgPath) Values ?', [data]);
        //     db.insertWithId(str, function(err, response) {
        //         if (err) {
        //             callback(err, null)
        //         }
        //         callback(null, attribute_ID);
        //     });
        // },
        function(resp1, attribute_id, callback) {

            console.log("fk_ptype_id", fk_ptype_id);
            console.log("attribute_id", attribute_id);
            console.log("resp1.insertId", resp1.insertId);



            var data = {
                fk_att_id: attribute_id,
                fk_ptype_id: fk_ptype_id
            };


            var str = mysql.format('INSERT into attribute_category_map SET ?', data);
            connection.query(str, function(err2, resp2) {
                        if (err2) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attribute_category_map');
                                callback(err2, null);
                            });

                        }
                        callback(null, attribute_id);
                    });
        }
    ], function(lasterr, attribute_id) {
                if (lasterr) {
                    next(lasterr, {
                        msg: 'fail saving record',
                        statuscode: '500',
                        success: false,
                    });
                } else {
                    connection.commit(function(err) {
                        if (err) {
                            connection.rollback(function() {
                                next(err, {
                                    msg: 'fail commiting transaction',
                                    statuscode: 500,
                                    success: false,
                                    errmsg: lasterr
                                });

                            });
                        } else {
                            next(null, {
                                msg: 'successfully adding record',
                                statuscode: 200,
                                success: true,
                                attribute_id: attribute_id
                            })
                        }
                    });
                }

            });

}
});

};

exports.checkAttributesNameIfExist = function checkSupplierNameIfExist(data, next) {
    console.log("CheckdataDao", data);
    var str = 'SELECT * FROM attributes WHERE `name` LIKE \'%' + data.name + '%\' LIMIT 1';
    db.query(str, next);
};

exports.getAttributes = function getAttributes(data, next) {
    //code goes here.

    db.query('SELECT * FROM attributes', next);
};

exports.getSpecificAttributes = function getSpecificAttributes(id, data, next) {
    //code goes here.
    console.log("attibutesSpecificDao", id);

    // db.query('SELECT * FROM attributes WHERE att_id =\'' + id + '\'', next);
    db.query('SELECT p.name AS pname, p.ptype_id AS productType_id, a.name, a.label, a.type, a.is_visible, a.is_required, a.is_variation, a.is_custumizable, a.is_unique, av.prod_att_id, av.value AS av_value, av.sku AS av_sku, av.value_image as av_image, av.name AS av_colorname, av.color AS av_color, av.is_single, av.is_image, av.is_video, av.is_audio, av.is_document FROM attributes a LEFT JOIN attribute_category_map acm ON a.att_id = acm.fk_att_id LEFT JOIN product_type p ON acm.fk_ptype_id = p.ptype_id LEFT JOIN attribute_value av ON attribute_id = att_id WHERE att_id =\'' + id + '\'', next);
    // db.query('SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_archived =\'0\' ORDER BY item_name;', next);
    
    // db.query('SELECT * FROM product p LEFT JOIN product_variant pv ON pv.prod_id = p.p_id WHERE p.p_id =\'' + id + '\' ORDER BY start_date DESC;', next);

};


exports.deleteAttributes = function deleteAttributes(id, next) {
    //code goes here.


    async.waterfall([
        function(callback) {
            var query = 'DELETE FROM attribute_category_map WHERE fk_att_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });

        },
        function(id, callback) {


            var query = 'DELETE FROM product_variant WHERE att_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {


            var query = 'DELETE FROM attribute_value WHERE attribute_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {


            var query = 'DELETE FROM attributes WHERE att_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        }
    ], next);


};


exports.updateAttributes = function updateAttributes(id, data, next) {

    console.log("Dao data", data);
    console.log("Dao id", id);


    // db.query('UPDATE attributes SET `name`=\'' + data.name + '\',label=\'' + data.label + '\',type=\'' + data.type + '\',default_value=\'' + data.default_value + '\',list_value=\'' + data.list_value + '\',sort_order=\'' + data.sort_order + '\',is_visible=\'' + data.is_visible + '\',is_search=\'' + data.is_search + '\',is_variation=\'' + data.is_variation + '\',is_required=\'' + data.is_required + '\' WHERE att_id =\'' + id + '\'', next);
    // db.query('SELECT p.name AS pname, p.ptype_id AS productType_id, a.name, a.label, a.type, a.is_visible, a.is_required, a.is_variation, a.is_custumizable, a.is_unique, av.prod_att_id, av.value AS av_value, av.sku AS av_sku, av.value_image as av_image, av.name AS av_colorname, av.color AS av_color, av.is_single, av.is_image, av.is_video, av.is_audio, av.is_document FROM attributes a LEFT JOIN attribute_category_map acm ON a.att_id = acm.fk_att_id LEFT JOIN product_type p ON acm.fk_ptype_id = p.ptype_id LEFT JOIN attribute_value av ON attribute_id = att_id WHERE att_id =\'' + id + '\'', next);
   

   var connection = mysql.createConnection({
        host: config.db_host,
        user: config.db_user,
        password: config.db_password,
        database: config.db_name,
        multipleStatements: true
    });


            //attribute
            var attributesObj = {
                name: data.name,
                label: data.label,
                type: data.type,
                is_visible: data.is_visible,
                is_required: data.is_required,
                is_variation: data.is_variation,
                is_custumizable: data.is_custumizable,
                is_unique: data.is_unique
            };

            //attribute value
            var val = data.val

            //attribute category map
            var fk_ptype_id = data.productType_id

            console.log("attibutesObj", attributesObj);
            console.log("val", val);
            console.log("fk_ptype_id", fk_ptype_id);




            connection.beginTransaction(function(err) {
        if (err) {
            next(err, {
                msg: 'fail connecting to server',
                statuscode: '500',
                success: false,
                errmsg: err
            })
        } else {

    async.waterfall([
        function(callback) {
            console.log("attributesObj", attributesObj);
            console.log("Dao ID", id);
            // var str = mysql.format('INSERT into attributes SET ?', attributesObj);
            // var str = mysql.format('UPDATE attributes SET `name`=\'' + attributesObj.name + '\',label=\'' + attributesObj.label + '\',type=\'' + attributesObj.type + '\',is_visible=\'' + attributesObj.is_visible + '\',is_required=\'' + attributesObj.is_required + '\',is_variation=\'' + attributesObj.is_variation + '\',is_required=\'' + attributesObj.is_required + '\',is_custumizable=\'' + attributesObj.is_custumizable + '\',
            //     is_unique = \'' + attributesObj.is_unique + '\' WHERE att_id =\'' + id + '\'');
            connection.query('UPDATE attributes SET `name`=\'' + attributesObj.name + '\',label=\'' + attributesObj.label + '\',type=\'' + attributesObj.type + '\',is_visible=\'' + attributesObj.is_visible + '\',is_required=\'' + attributesObj.is_required + '\',is_variation=\'' + attributesObj.is_variation + '\',is_required=\'' + attributesObj.is_required + '\',is_custumizable=\'' + attributesObj.is_custumizable + '\',is_unique = \'' + attributesObj.is_unique + '\' WHERE att_id =\'' + id + '\'', function(err0, resp0) {
                        if (err0) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attributes');
                                callback(err0, null);
                            });

                        }
                        callback(null, resp0);
                    });
        },function(resp0, callback) {
            // var attribute_id = resp0.insertId;
            // console.log("id")
            var str = 'DELETE FROM attribute_value WHERE attribute_id =\'' + id + '\'';

            connection.query(str, function(errDel, respDel) {
                        if (errDel) {
                            connection.rollback(function() {
                                console.log('err : DELETE into attribute_value');
                                callback(errDel, null);
                            });

                        }
                        callback(null, resp0);
                    });

        },function(resp0, callback) {
            console.log("attibutes resp", resp0);
            // var attribute_id = resp0.insertId;

            var data = [];
            console.log("attributesObj.type", attributesObj.type);
            if(attributesObj.type == 'Color Picker'){

                val.forEach(function(obj) {

                var temp = [];
                temp.push(id);
                temp.push(124);
                temp.push(obj.name);
                temp.push(obj.color);

                data.push(temp);
            });

                console.log("attribute_value Object", data);
            var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, name, color) Values ?', [data]);

            }else if(attributesObj.type == 'Attachment'){

                  console.log("attribute_value Attachment Object", val);

                  var attachmentObj = {
                    attribute_id: id,
                    product_id: 124,
                    is_single: val.is_single,
                    is_image: val.attchtype.attachment_Image,
                    is_video: val.attchtype.attachment_Video,
                    is_audio: val.attchtype.attachment_Audio,
                    is_document: val.attchtype.attachment_Document
                  };

            var str = mysql.format('INSERT into attribute_value SET ?', attachmentObj);


            

            }else{

                val.forEach(function(obj) {

                var temp = [];
                temp.push(id);
                temp.push(124);
                temp.push(obj.value);
                temp.push(obj.sku);
                temp.push(obj.value_image);

                data.push(temp);
            });

                console.log("attribute_value Object", data);
            var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);

            }
            


            // console.log("attribute_value Object", data);
            // var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);
            connection.query(str, function(err1, resp1) {
                        if (err1) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attribute_value');
                                callback(err0, null);
                            });

                        }
                        callback(null, resp1, id);
                    });
        },function(resp1, attribute_id, callback) {

            console.log("fk_ptype_id", fk_ptype_id);
            console.log("attribute_id", attribute_id);
            // console.log("resp1.insertId", resp1.insertId);



            // var data = {
            //     fk_att_id: attribute_id,
            //     fk_ptype_id: fk_ptype_id
            // };


            // var str = mysql.format('Update attribute_category_map SET ?', data);
            connection.query('Update attribute_category_map SET fk_ptype_id = \'' + fk_ptype_id + '\' WHERE fk_att_id =\'' + attribute_id + '\'', function(errDEL2, respDEL2) {
                        if (errDEL2) {
                            connection.rollback(function() {
                                console.log('err : Update into attribute_category_map');
                                callback(err2, null);
                            });

                        }
                        callback(null, attribute_id);
                    });
        }], function(lasterr, attribute_id) {
                if (lasterr) {
                    next(lasterr, {
                        msg: 'fail saving record',
                        statuscode: '500',
                        success: false,
                    });
                } else {
                    connection.commit(function(err) {
                        if (err) {
                            connection.rollback(function() {
                                next(err, {
                                    msg: 'fail commiting transaction',
                                    statuscode: 500,
                                    success: false,
                                    errmsg: lasterr
                                });

                            });
                        } else {
                            next(null, {
                                msg: 'successfully adding record',
                                statuscode: 200,
                                success: true,
                                attribute_id: attribute_id
                            })
                        }
                    });
                }

            });

}

});
        // function(resp0, callback) {
        //     console.log("attibutes resp", resp0);
        //     var attribute_id = resp0.insertId;

        //     var data = [];
        //     console.log("attributesObj.type", attributesObj.type);
        //     if(attributesObj.type == 'Color Picker'){

        //         val.forEach(function(obj) {

        //         var temp = [];
        //         temp.push(attribute_id);
        //         temp.push(124);
        //         temp.push(obj.name);
        //         temp.push(obj.color);

        //         data.push(temp);
        //     });

        //         console.log("attribute_value Object", data);
        //     var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, name, color) Values ?', [data]);

        //     }else if(attributesObj.type == 'Attachment'){

        //           console.log("attribute_value Attachment Object", val);

        //           var attachmentObj = {
        //             attribute_id: attribute_id,
        //             product_id: 124,
        //             is_single: val.is_single,
        //             is_image: val.attchtype.attachment_Image,
        //             is_video: val.attchtype.attachment_Video,
        //             is_audio: val.attchtype.attachment_Audio,
        //             is_document: val.attchtype.attachment_Document
        //           };

        //     var str = mysql.format('INSERT into attribute_value SET ?', attachmentObj);


            

        //     }else{

        //         val.forEach(function(obj) {

        //         var temp = [];
        //         temp.push(attribute_id);
        //         temp.push(124);
        //         temp.push(obj.value);
        //         temp.push(obj.sku);
        //         temp.push(obj.value_image);

        //         data.push(temp);
        //     });

        //         console.log("attribute_value Object", data);
        //     var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);

        //     }
            


        //     // console.log("attribute_value Object", data);
        //     // var str = mysql.format('INSERT into attribute_value (attribute_id, product_id, value, sku, value_image) Values ?', [data]);
        //     connection.query(str, function(err1, resp1) {
        //                 if (err1) {
        //                     connection.rollback(function() {
        //                         console.log('err : INSERT into attribute_value');
        //                         callback(err0, null);
        //                     });

        //                 }
        //                 callback(null, resp1, attribute_id);
        //             });
        // },
        // function(attribute_ID, attributeValue_ID, val, callback) {

        //     var data = [];
        //     val.forEach(function(obj) {
        //         var temp = [];
        //         temp.push(attributeValue_ID);
        //         temp.push(attribute_ID);
        //         temp.push(124);
        //         temp.push(obj.sku);
        //         temp.push(obj.image);

        //         data.push(temp);


        //     });

        //     var str = mysql.format('INSERT into product_variant (attributeValue_id, att_id, prod_id,sku, product_imgPath) Values ?', [data]);
        //     db.insertWithId(str, function(err, response) {
        //         if (err) {
        //             callback(err, null)
        //         }
        //         callback(null, attribute_ID);
        //     });
        // },
        // function(resp1, attribute_id, callback) {

        //     console.log("fk_ptype_id", fk_ptype_id);
        //     console.log("attribute_id", attribute_id);
        //     console.log("resp1.insertId", resp1.insertId);



        //     var data = {
        //         fk_att_id: attribute_id,
        //         fk_ptype_id: fk_ptype_id
        //     };


        //     var str = mysql.format('INSERT into attribute_category_map SET ?', data);
        //     connection.query(str, function(err2, resp2) {
        //                 if (err2) {
        //                     connection.rollback(function() {
        //                         console.log('err : INSERT into attribute_category_map');
        //                         callback(err2, null);
        //                     });

        //                 }
        //                 callback(null, attribute_id);
        //             });
        // }
    






};
