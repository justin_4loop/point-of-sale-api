'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var bcrypt = require('bcrypt-nodejs');

exports.createEmployee = function createEmployee(data, next) {
	var employees = {};
	employees.lastname = data.lastname;
	employees.firstname = data.firstname;
	employees.isActive = data.isActive;
	employees.store_id = data.store_id;

	// i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
            var str = db.insertBulkWithId('INSERT into employees SET ?', employees, function(err1, result) {
                if (err1) {
                    cb(err1, null);
                } else {
                    cb(null, result);
                }
            });
        }
    ], next);
};

exports.getEmployee = function getEmployee(next) {
    db.query('SELECT * from employees', next);
};

exports.getEmployeeWithoutAccount = function getEmployeeWithoutAccount(next) {
    db.query('select e.* from employees e where e.emp_id not in (select u.emp_id from user_accounts u)', next);
};
exports.getEmployeeById = function getEmployeeById(id, next) {
	console.log('id->',id);
    db.query('SELECT * from employees WHERE employees.emp_id =\'' + id + '\'', next);
};

exports.deleteEmployee = function deleteEmployee(id, next) {
    var query = 'DELETE FROM employees WHERE emp_id =\'' + id + '\'';
    db.actionQuery(query, next);
};

exports.updateEmployee = function updateEmployee(id,data, next) {
    
    var employees = {};
	employees.lastname = data.lastname;
	employees.firstname = data.firstname;
	employees.isActive = data.isActive;
	employees.store_id = data.store_id;
	console.log(employees);

	// i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
        	db.insertBulkWithId('UPDATE employees SET ? WHERE emp_id = \'' + id + '\'', employees, function(err1,result){
        		if(err1){
        			cb(err1,null);
        		}else{
        			cb(null,id);
        		}
        	});
        }
    ], next);
    
};
