'use strict';
var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');

exports.saveCategories = function saveCategories(data, next) {
    console.log("Dao", data);
    //code goes here.

    if (data.cat_id == '') {

        var category = {
            name: data.name,
            label: data.label,
            description: data.description,
            c_image: data.c_image
        };

    } else {

        var category = {
            parent_id: data.cat_id,
            name: data.name,
            label: data.label,
            description: data.description,
            c_image: data.c_image
        };
    }

    // console.log("category", category);

    var str = mysql.format('INSERT into categories SET ?', category);
    db.insertWithId(str, next);




};

exports.checkCategoryNameIfExist = function checkSupplierNameIfExist(data, next) {
    console.log("data", data);
    var str = 'SELECT * FROM categories WHERE `name` LIKE \'%' + data.name + '%\' LIMIT 1';
    db.query(str, next);
};


exports.getChildCategories = function getChildCategories(id, data, next) {
    //code goes here.

    db.query('SELECT c_id AS id, name, parent_id FROM categories WHERE parent_id =\'' + id + '\'', next);

};

exports.getSpecificCategory = function getSpecificCategory(id, data, next) {
    //code goes here.

    db.query('SELECT * FROM categories WHERE c_id =\'' + id + '\'', next);

};

exports.getCategories = function getCategories(data, next) {
    //code goes here.
    console.log("Dao Catgories");
    db.query('SELECT c_id AS id, name, parent_id FROM categories', next);
};

exports.deleteCategories = function deleteCategories(id, next) {
    //code goes here.

    var query = 'DELETE FROM categories WHERE c_id =\'' + id + '\'';
    db.actionQuery(query, next);
};


exports.updateCategories = function updateCategories(id, data, next) {
    //code goes here.
    db.query('UPDATE categories SET `name`=\'' + data.name + '\',label=\'' + data.label + '\' ,parent_id=\'' + data.parent_id + '\',description=\'' + data.description + '\', c_image=\'' + data.c_image + '\' WHERE c_id =\'' + id + '\'', next);

};
