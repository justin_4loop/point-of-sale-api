'use strict';
var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');

exports.saveStore = function saveStore(data, next) {
    //code goes here.
    console.log(data);
    var str = mysql.format('INSERT into store_info SET ?', data);
    db.query(str, next);

};

exports.checkStoreNameIfExist = function checkStoreNameIfExist(data, next) {
    var str = 'SELECT * FROM store_info WHERE `name` LIKE \'%' + data.name + '%\' LIMIT 1';
    db.query(str, next);
};

exports.getStore = function getStore(data, next) {
    //code goes here.

    db.query('SELECT * FROM store_info', next);
};


exports.getSpecificStore = function getStore(id, data, next) {
    //code goes here.

    db.query('SELECT * FROM store_info WHERE store_id =\'' + id + '\'', next);
};



exports.deleteStore = function deleteStore(id, next) {
    //code goes here.

    async.waterfall([
        function(callback) {

            // console.log(data);
            var query = 'DELETE FROM price_history WHERE store_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });

        },
        function(id, callback) {


            var query = 'DELETE FROM store_info WHERE store_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        }
    ], next);

};


exports.updateStore = function updateStore(id, data, next) {
    console.log("Dao ID", id);
    console.log("Dao Data", data);

    var str = mysql.format('UPDATE store_info SET ? WHERE store_id =\'' + id + '\'', data);
    db.query(str, next);


};
