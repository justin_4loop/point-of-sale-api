'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var bcrypt = require('bcrypt-nodejs');

exports.createUser = function createUser(data, next) {
    var user_accounts = {};
    user_accounts.uc_id = null;
    user_accounts.username = data.username;
    //user_accounts.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    user_accounts.password = data.password;
    user_accounts.emp_id = data.emp_id;
    user_accounts.role_id = data.role_id;
    user_accounts.isStatus = 0;
    user_accounts.valid_until = new Date(data.valid_until).toISOString().slice(0, 19).replace('T', ' ');
    
    // i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
            var str = mysql.format('INSERT into user_accounts SET ?', user_accounts);
            console.log('str: ', str);
            db.insertWithId(str, function(err2, result) {
                console.log('err2: ', err2);
                if (err2) {
                    cb(err2, null);
                } else {
                    cb(null, result);
                }
            });
        }
    ], next);
};

exports.getUser = function getUser(next) {
    db.query('SELECT U.uc_id,U.username,U.valid_until,E.emp_id,U.isStatus,U.role_id,CONCAT(E.lastname,", ",E.firstname) AS EmpName,E.isActive,E.store_id,(SELECT `name` FROM store_info WHERE store_id = E.store_id LIMIT 1) AS StoreInfo FROM user_accounts U INNER JOIN employees E ON U.emp_id = E.emp_id;', next);
};

exports.getUserById = function getUserById(id, next) {
    console.log('id->', id);
    db.query('SELECT * from user_accounts WHERE uc_id =\'' + id + '\'', next);
};

exports.deleteUser = function deleteUser(id, next) {
    var query = 'DELETE FROM user_accounts WHERE uc_id =\'' + id + '\'';
    db.actionQuery(query, next);
};

exports.updateUser = function updateUser(id, data, next) {
    var user_accounts = {};
    user_accounts.username = data.username;
    //user_accounts.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    user_accounts.password = data.password;
    user_accounts.emp_id = data.emp_id;
    user_accounts.role_id = data.role_id;
    user_accounts.valid_until = new Date(data.valid_until).toISOString().slice(0, 19).replace('T', ' ');
    console.log('user_accounts: ',user_accounts);

    // i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
             var str = mysql.format('UPDATE user_accounts SET ? WHERE uc_id = \'' + id + '\'', user_accounts);            
            db.actionQuery(str, function(err2, result) {
                if (err2) {
                    cb(err2, null);
                } else {
                    cb(null, result);
                }
            });
        }
    ], next);

};

exports.checkUserNameIfExist = function checkUserNameIfExist(username, next) {
    var str = 'SELECT * FROM user_accounts WHERE `username` LIKE \'%' + username + '%\' LIMIT 1';
    db.query(str, next);
};

exports.checkEmployeeIfExist = function checkEmployeeIfExist(id, next) {
    var str = 'SELECT * FROM user_accounts WHERE `emp_id` =\'' + id + '\' LIMIT 1';
    db.query(str, next);
};