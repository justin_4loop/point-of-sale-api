'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var functions = require('../../app/utils/functions');
var async = require('async');
var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash-node');


function denyWriteMsg() {
    return {
        msg: 'Sorry you have NO WRITE privilege in this module. <br> If this should not be case, please inform your Database Administrator.',
        header: 'Limited Privileges Only'
    }
}

function denyReadMsg() {
    return {
        msg: 'Sorry you have NO READ privilege in this module. <br> If this should not be case, please inform your Database Administrator.',
        header: 'Limited Privileges Only'
    }
}

function denyPrintMsg() {
    return {
        msg: 'Sorry you have NO PRINT privilege in this module. <br> If this should not be case, please inform your Database Administrator.',
        header: 'Limited Privileges Only'
    }
}

function denyDeleteMsg() {
    return {
        msg: 'Sorry you have NO DELETE privilege in this module. <br> If this should not be case, please inform your Database Administrator.',
        header: 'Limited Privileges Only'
    }
}


exports.createUserGroup = function createUserGroup(data, next) {

    var group = {};
    group.name = data.name;
    group.description = data.description;

    // i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
            var str = db.insertBulkWithId('INSERT INTO user_roles SET ?', group, function(err2, result) {
                if (err2) {
                    cb(err2, null);
                } else {
                    console.log('result: ', result);
                    cb(null, result);
                }
            });
        },
        function(role_id, callback) {
            var str = db.actionQuery('INSERT INTO user_privs SELECT null,\'' + role_id + '\',mf_id,M.DefaultRead,M.DefaultWrite,M.DefaultDelete,M.DefaultPrint FROM module_item M WHERE M.inactive <> 1', function(err2, result) {
                if (err2) {
                    callback(err2, null);
                } else {
                    console.log('result2: ', result);
                    callback(null, result);
                }
            });
        }
    ], next);
};

exports.checkGroupNameIfExist = function checkGroupNameIfExist(data, next) {
    var str = 'SELECT * FROM user_roles WHERE `name` LIKE \'%' + data.name + '%\' LIMIT 1';
    db.query(str, next);
};

exports.getAllUserGroups = function getAllUserGroups(next) {
    db.query('SELECT * FROM user_roles;', next);
};

exports.getUserGroupById = function getUserGroupById(id, next) {
    console.log('id->', id);
    db.query('SELECT * from user_roles WHERE role_id =\'' + id + '\'', next);
};

exports.deleteUserGroup = function deleteUserGroup(id, next) {
    var query = 'DELETE FROM user_roles WHERE role_id =\'' + id + '\'';
    db.actionQuery(query, next);
};

exports.updateUserGroup = function updateUserGroup(id, data, next) {
    var group = {};
    group.name = data.name;
    group.description = data.description;

    // i am using this structure(async waterfall) for later purposes
    async.waterfall([
        function(cb) {
            db.insertBulkWithId('UPDATE user_roles SET ? WHERE role_id = \'' + id + '\'', group, function(err2, result) {
                if (err2) {
                    cb(err2, null);
                } else {
                    cb(null, result);
                }
            });
        }
    ], next);

};

exports.setGroupPrivilege = function setGroupPrivilege(role_id, data, next) {
    if (!_.isEmpty(data)) {
        var i = 1;
        _.each(data, function(product) {
            if (_.isBoolean(product.DefaultRead)) {
                product.DefaultRead = functions.BooleanToInt(product.DefaultRead);
            }

            if (_.isBoolean(product.DefaultWrite)) {
                product.DefaultWrite = functions.BooleanToInt(product.DefaultWrite);
            }

            if (_.isBoolean(product.DefaultDelete)) {
                product.DefaultDelete = functions.BooleanToInt(product.DefaultDelete);
            }

            if (_.isBoolean(product.DefaultPrint)) {
                product.DefaultPrint = functions.BooleanToInt(product.DefaultPrint);
            }

            db.insertBulkWithId('UPDATE user_privs SET `Read`=?,`Write`=?,`Delete`=?,`Print`=? WHERE module_item_id=?',
                                [product.DefaultRead,product.DefaultWrite,product.DefaultDelete,product.DefaultPrint, product.mf_id],
            function(err2, result) {
                if (err2) {
                    console.log(err2);
                    return;
                }
                
                if(i === data.length){
                    console.log("Completed ", i);
                    next(null,i);
                }
                console.log("Set label on row %s", product.mf_id);
                console.log("Set i on %i", i);
                i++;
            });
        });
    }


};