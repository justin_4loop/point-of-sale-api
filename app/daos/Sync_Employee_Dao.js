'use strict';
var mysql = require('mysql');
var async = require('async');
var _ = require('lodash-node');
var archiver = require('archiver');
var fs = require('fs-extra');
var path = require('path');
var Database = require('../../app/utils/database').Database;
var Helper = require('../daos/Sync_Helper_Dao');
var db = new Database();

var me = this;

/** *************************** E M P L O Y E E *************************  **/

exports.getStores = function getStores(devicename, next) {
    db.query('SELECT si.* FROM device d ' +
        'INNER JOIN device_map dm ON d.device_id = dm.device_id ' +
        'INNER JOIN store_info si ON si.store_id = dm.store_id ' +
        'WHERE d.name = \'' + devicename + '\' LIMIT 1; ', function(err, store){
        	Helper.getLogs(devicename, store[0].store_id, "store_info", "INSERT", "EMPLOYEELOGS", function(err, logs){
        		var qry = mysql.format('SELECT * FROM store_info WHERE store_id IN (?);', [logs]);

        		db.query(qry, next);
        	});
        });
};

exports.getstore = function getstore(devicename, next) {
    db.query('SELECT si.* FROM device d ' +
        'INNER JOIN device_map dm ON d.device_id = dm.device_id ' +
        'INNER JOIN store_info si ON si.store_id = dm.store_id ' +
        'WHERE d.name = \'' + devicename + '\' LIMIT 1; ', next)
};

exports.getEmployees = function getEmployees(devicename, next) {
    me.getstore(devicename, function(err, store) {
        Helper.getLogs(devicename, store[0].store_id, "Employees", "INSERT", "EMPLOYEELOGS" ,function(err, logs) {
            var qry = mysql.format('SELECT * FROM Employees WHERE emp_id IN (?) AND store_id = ?;', [logs, store[0].store_id]);
            db.query(qry, function(err, data){
            	Helper.ZipImages(data, devicename, "employees", "EMPLOYEELOGS", function(err, zipRes){
            		next(null, data);
            	});
            });
        });
    });
};

exports.getUserAccounts = function getUserAccounts(devicename, next) {
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "user_accounts", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM user_accounts WHERE uc_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

exports.getUserAccess = function getUserAccess(devicename, next) {
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "user_access", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM user_access WHERE access_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

exports.getUserRoles = function getUserRoles(devicename, next){
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "user_roles", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM user_roles WHERE role_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

exports.getUserPrivs = function getUserPrivs(devicename, next) {
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "user_privs", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM user_privs WHERE up_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

exports.getModuleItems = function getModuleItems(devicename, next) {
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "module_item", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM module_item WHERE mf_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

exports.getModules = function getModules(devicename, next) {
    me.getstore(devicename, function(err, store){
        Helper.getLogs(devicename, store[0].store_id, "modules", "INSERT", "EMPLOYEELOGS" ,function(err, logs){
            var qry = mysql.format('SELECT * FROM modules WHERE module_id IN (?);', [logs]);

            db.query(qry, next);
        });
    });
};

/** *************************** E N D  E M P L O Y E E *************************  **/