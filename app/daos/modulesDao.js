'use strict';

var Database = require('../../app/utils/database').Database;
var db = new Database();


exports.getAllModules = function getAllModules(next) {
    var sql = 'SELECT * FROM modules;';
    db.query(sql, next);
};

exports.getAllModuleItems = function getAllModules(next) {
    var sql = 'SELECT * FROM module_item;';
    db.query(sql, next);
};

exports.getAllModulesPermissions = function getAllModulesPermissions(next){
	var sql = 'SELECT MO.module_id, MO.name, ME.item_name, ME.item_description, ME.DefaultRead, ME.DefaultWrite, ME.DefaultDelete, ME.DefaultPrint, ME.Inactive, ME.mf_id FROM modules MO LEFT JOIN module_item ME ON ME.module_id = MO.module_id ORDER BY MO.module_id';
	db.query(sql, next);
};

exports.generateAccessPermission = function generateAccessPermission(id,next){
	var sql = 'SELECT GP.role_id,MO.module_id,MO.`name`,ME.item_name,ME.item_description,GP.`Read` AS DefaultRead,GP.`Write` AS DefaultWrite,GP.`Delete` AS DefaultDelete,GP.Print AS DefaultPrint,ME.Inactive,ME.mf_id FROM module_item ME LEFT JOIN modules MO ON ME.module_id = MO.module_id LEFT JOIN user_privs GP ON GP.module_item_id = ME.mf_id WHERE GP.role_id = \'' + id + '\' ORDER BY MO.module_id,mf_id;';
	db.query(sql, next);
};