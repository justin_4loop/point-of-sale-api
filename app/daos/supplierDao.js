'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();


exports.saveSupplier = function saveSupplier(data, next) {
    //code goes here.
    var str = mysql.format('INSERT into supplier SET ?', data);
    db.query(str, next);
};

exports.checkSupplierNameIfExist = function checkSupplierNameIfExist(data, next) {
    var str = 'SELECT * FROM supplier WHERE `name` LIKE \'%' + data.name + '%\' LIMIT 1';
    db.query(str, next);
};

exports.getSupplier = function getSupplier(data, next) {
    //code goes here.
    db.query('SELECT * FROM supplier', next);
};


exports.deleteSupplier = function deleteSupplier(id, next) {
    //code goes here.
    var query = 'DELETE FROM supplier WHERE s_id =\'' + id + '\'';
    db.actionQuery(query, next);

};


exports.updateSupplier = function updateSupplier(id, data, next) {
    db.query('UPDATE supplier SET `name`=\'' + data.name + '\',contact_person=\'' + data.contact_person + '\',contact_number=\'' + data.contact_number + '\',address=\'' + data.address + '\',isActive=\'' + data.isActive + '\' WHERE s_id =\'' + id + '\'', next);
};
