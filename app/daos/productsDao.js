'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var moment = require('moment');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);
var functions = require('../../app/utils/functions');

var connection = mysql.createConnection({
    host: config.db_host,
    user: config.db_user,
    password: config.db_password,
    database: config.db_name,
    multipleStatements: true
});


/*exports.saveProduct = function saveProduct(data, next) {
    //data.start_date = moment(data.start_date).format('MM-DD-YYYY');
    //data.end_date = moment(data.end_date).format('MM-DD-YYYY');
    var cat_id = data.category_id;
    var stor_id = data.store_id;
    var att_id = data.att_id;
    var ph_id = data.ph_id;
    var ptype_id = data.ptype_id;

    //for attrib value
    var attribute_value = data.attribute_value;
    var attribute_value_sku = data.attribute_value_sku;
    var attribute_value_image = data.attribute_value_image;

    // for product variant
    var product_variant_sku = data.product_variant_sku;
    var product_variant_imgPath = data.product_variant_imgPath;

    // for inventory
    var status = data.inv_status;
    var stock_onhand = data.inv_stock_onhand;
    var reorder_level = data.inv_reorder_level;
    var outOfStock_reason = data.inv_outOfStock_reason;
    var reStock_date = data.inv_reStock_date;

    connection.beginTransaction(function(err) {
        if (err) {
            next(err, {
                msg: 'fail connecting to server',
                statuscode: '500',
                success: false,
                errmsg: err
            })
        } else {
            var productObj = {
                item_name: data.item_name,
                item_description: data.item_description,
                sell_price: data.sell_price,
                purchase_price: data.purchase_price,
                vat: data.vat,
                reorder_level: reorder_level,
                productType_id: ptype_id,
                supplier_id: data.supplier_id,
                start_date: data.start_date,
                end_date: data.end_date,
                is_published: data.is_published,
                product_code: data.product_code,
                is_archived: data.is_archived,
                brand: data.brand
            };

            async.waterfall([
                function(callback) {
                    var str = mysql.format('INSERT into product SET ?', productObj);
                    connection.query(str, function(err0, resp0) {
                        if (err0) {
                            connection.rollback(function() {
                                console.log('err : INSERT into product');
                                callback(err0, null);
                            });

                        }
                        callback(null, resp0);
                    });
                },
                function(resp0, callback) {
                    var attribvalObj = {
                        product_id: resp0.insertId,
                        attribute_id: att_id,
                        priceHistory_id: ph_id,
                        value: attribute_value,
                        sku: attribute_value_sku,
                        value_image: attribute_value_image
                    };
                    var str = mysql.format('INSERT into attribute_value SET ?', attribvalObj);
                    connection.query(str, function(err1, resp1) {
                        if (err1) {
                            connection.rollback(function() {
                                console.log('err : INSERT into attribute_value');
                                callback(err1, null);
                            });
                        }
                        callback(null, resp0.insertId, resp1.insertId);
                    });
                },
                function(product_ID, attributeValue_ID, callback) {

                    var prodvarianObj = {
                        prod_id: product_ID,
                        attributeValue_id: attributeValue_ID,
                        att_id: att_id,
                        ph_id: ph_id,
                        ptype_id: ptype_id,
                        sku: product_variant_sku,
                        product_imgPath: product_variant_imgPath
                    };
                    var str = mysql.format('INSERT into product_variant SET ?', prodvarianObj);
                    connection.query(str, function(err2, resp2) {
                        if (err2) {
                            connection.rollback(function() {
                                console.log('err : INSERT into product_variant');
                                callback(err2, null);
                            });
                        }
                        callback(null, product_ID);
                    });
                },
                function(product_ID, callback) {

                    var invObj = {
                        fk_p_id: product_ID,
                        fk_store_id: stor_id,
                        status: status,
                        stock_onhand: stock_onhand,
                        reorder_level: reorder_level,
                        outOfStock_reason: outOfStock_reason,
                        reStock_date: reStock_date
                    };
                    var str = mysql.format('INSERT into inventory SET ?', invObj);
                    connection.query(str, function(err3, resp3) {
                        if (err3) {
                            connection.rollback(function() {
                                console.log('err : INSERT into inventory');
                                callback(err3, null);
                            });
                        }
                        callback(null, product_ID);
                    });
                },
                function(product_ID, callback) {

                    var prodCatObj = {
                        fk_product_id: product_ID,
                        fk_category_id: cat_id
                    };
                    var str = mysql.format('INSERT into product_category SET ?', prodCatObj);
                    connection.query(str, function(err4, resp4) {
                        if (err4) {
                            connection.rollback(function() {
                                console.log('err : INSERT into product_category');
                                callback(err4, null);
                            });
                        }
                        callback(null, product_ID);
                    });
                }
            ], function(lasterr, prodId) {
                if (lasterr) {
                    next(lasterr, {
                        msg: 'fail saving record',
                        statuscode: '500',
                        success: false,
                    });
                } else {
                    connection.commit(function(err) {
                        if (err) {
                            connection.rollback(function() {
                                next(err, null);

                            });
                        } else {
                            next(null, {
                                msg: 'Record Successfully Saved',
                                success: true,
                                result: {
                                    prodId: prodId
                                }
                            })
                        }
                    });
                }

            });

        }
    });
};*/

exports.saveProduct = function saveProduct(data, next) {

    connection.beginTransaction(function(err) {
        if (err) {
            next(err, {
                msg: 'fail connecting to server',
                statuscode: '500',
                success: false,
                errmsg: err
            })
        } else {
            async.waterfall([
                function(callback) {
                    var productObj = {
                        item_name: data.item_name,
                        item_description: data.item_description,
                        sell_price: data.sell_price,
                        purchase_price: data.purchase_price,
                        vat: data.vat,
                        reorder_level: data.inv_reorder_level,
                        productType_id: data.ptype_id,
                        start_date: new Date(data.start_date).toISOString().slice(0, 19).replace('T', ' '),
                        end_date: new Date(data.end_date).toISOString().slice(0, 19).replace('T', ' '),
                        is_published: data.is_published,
                        product_code: data.product_code,
                        is_archived: data.is_archived,
                        brand: data.brand
                    };
                    var str = mysql.format('INSERT into product SET ?', productObj);
                    connection.query(str, function(err0, resp0) {
                        if (err0) {
                            return connection.rollback(function() {
                                console.log('err : INSERT into product: ', err0);
                                next(err0, null);
                            });
                        }
                        callback(null, resp0.insertId)
                    });
                },
                function(product_id, callback) {
                    var prodvarianObj = {
                        prod_id: product_id,
                        att_id: data.att_id,
                        ptype_id: data.ptype_id,
                        sku: data.product_variant_sku || 'Default variant sku',
                        product_imgPath: data.product_variant_imgPath || 'http://placehold.it/250x250'
                    };

                    var str = mysql.format('INSERT into product_variant SET ?', prodvarianObj);
                    connection.query(str, function(err2, resp2) {
                        if (err2) {
                            return connection.rollback(function() {
                                console.log('err : INSERT into product_variant');
                                next(err2, null);
                            });
                        }
                        callback(null, resp2.insertId, product_id);
                    });
                }
            ], function(lasterr, variant_id, prodId) {
                connection.commit(function(err) {
                    if (err) {
                        connection.rollback(function() {
                            next(err, null);
                        });
                    } else {
                        next(null, {
                            msg: 'Record Successfully Saved',
                            success: true,
                            result: {
                                productID: prodId,
                                variantID: variant_id
                            }
                        })
                    }
                });
            });
        }
    });
};


exports.updateProduct = function updateProduct(id, data, next) {
    data.is_published = functions.BooleanToInt(data.is_published);
    data.is_archived = functions.BooleanToInt(data.is_archived);
    data.start_date = new Date(data.start_date).toISOString().slice(0, 19).replace('T', ' ');
    data.end_date = new Date(data.end_date).toISOString().slice(0, 19).replace('T', ' ');

    connection.beginTransaction(function(err) {
        if (err) {
            next(err, {
                msg: 'fail connecting to server',
                statuscode: '500',
                success: false,
                errmsg: err
            })
        }

        async.waterfall([
            function(callback) {
                var productObj = {
                    item_name: data.item_name,
                    item_description: data.item_description,
                    sell_price: data.sell_price,
                    purchase_price: data.purchase_price,
                    vat: data.vat,
                    reorder_level: data.inv_reorder_level,
                    productType_id: data.ptype_id,
                    start_date: new Date(data.start_date).toISOString().slice(0, 19).replace('T', ' '),
                    end_date: new Date(data.end_date).toISOString().slice(0, 19).replace('T', ' '),
                    is_published: data.is_published,
                    product_code: data.product_code,
                    is_archived: data.is_archived,
                    brand: data.brand
                };
                var str = mysql.format('UPDATE product SET ? WHERE p_id =\'' + id + '\'', productObj);
                connection.query(str, function(err0, resp0) {
                    if (err0) {
                        return connection.rollback(function() {
                            console.log('err : UPDATE product SET: ', err0);
                            next(err0, null);
                        });
                    }
                    callback(null, resp0.changedRows)
                });
            },
            function(changedRows, callback) {
                var prodvarianObj = {
                    prod_id: id,
                    att_id: data.att_id,
                    ptype_id: data.ptype_id,
                    sku: data.product_variant_sku || 'Default variant sku',
                    product_imgPath: data.product_variant_imgPath || 'http://placehold.it/250x250'
                };

                var str = mysql.format('UPDATE product_variant SET ? WHERE pv_id=\'' + data.pv_id + '\'', prodvarianObj);
                connection.query(str, function(err2, resp2) {
                    if (err2) {
                        return connection.rollback(function() {
                            console.log('err : UPDATE product_variant');
                            next(err2, null);
                        });
                    }
                    callback(null, resp2.changedRows, changedRows);
                });
            }
        ], function(lasterr, variant_id, prodId) {
            connection.commit(function(err) {
                if (err) {
                    connection.rollback(function() {
                        next(err, null);
                    });
                } else {
                    next(null, {
                        msg: 'Record Successfully Updated',
                        success: true,
                        result: {
                            product_changed: prodId,
                            variant_changed: variant_id
                        }
                    })
                }
            });
        });
    });


};



exports.getProducts = function getProducts(next) {
    console.log("Hello Products");
    db.query('SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_archived =\'0\' ORDER BY item_name;', next);
    // db.query("SELECT p.item_name, pt.name, p.sell_price, p.start_date FROM product p JOIN product_type pt ON p.productType_id = pt.ptype_id ORDER BY start_date DESC;", next);

};

exports.getProduct = function getProduct(id, next) {
    db.query('SELECT * FROM product p LEFT JOIN product_variant pv ON pv.prod_id = p.p_id WHERE p.p_id =\'' + id + '\' ORDER BY start_date DESC;', next);
};


exports.deleteProduct = function deleteProduct(id, next) {
    async.waterfall([
        function(callback) {
            var query = 'DELETE FROM inventory WHERE fk_p_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {
            var query = 'DELETE FROM product_category WHERE fk_product_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {
            var query = 'DELETE FROM product_variant WHERE prod_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {
            var query = 'DELETE FROM attribute_value WHERE product_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {

            console.log(id);
            var query = 'DELETE FROM price_history WHERE fk_product_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {

                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        },
        function(id, callback) {
            var query = 'DELETE FROM product WHERE p_id =\'' + id + '\'';
            db.actionQuery(query, function(err, response) {
                if (err) {
                    callback(err, null)
                }
                callback(null, id);
            });
        }
    ], next);
};


exports.searchProduct = function searchProduct(data, next) {
    // console.log(data);
    console.log("Dao Search");
    console.log(data);
    var query = 'SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where p.item_name like \'' + data + '%\' ORDER BY start_date DESC;'
    db.query(query, next);

    // db.query("SELECT pv.product_imgPath, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where p.item_name =\'' + data.is_archived + '\' ORDER BY start_date DESC;", next);
    // var query = 'SELECT * from product where item_name =\'' + data + '\'';
};


exports.productCategoryFilter = function productCategoryFilter(data, next) {

    var query = 'SELECT * from product where category_id =\'' + data + '\'';
    db.query(query, next);

};


exports.archiveProduct = function archiveProduct(id, data, next) {

    console.log("Controller");


    console.log("id", id);
    console.log("data", data);

    var is_archived = 0;


    if (id == 0) {
        is_archived = 0;

        var str = "";
        data.forEach(function(item) {

            console.log("item", item);
            str += "UPDATE product SET is_archived = " + is_archived + " Where p_id=" + item + "; "
        });

        db.query(str, next);

    } else {

        is_archived = 1;


        var str = "";
        data.forEach(function(item) {

            console.log("item", item);
            str += "UPDATE product SET is_archived = " + is_archived + " Where p_id=" + item + "; "
        });

        db.query(str, next);

    }

};


exports.publishProduct = function publishProduct(id, data, next) {

    console.log("id", id);
    console.log("data", data);


    var is_published = 0;


    if (id == 1) {
        is_published = 1;

        var str = "";
        data.forEach(function(item) {

            console.log("item", item);
            str += "UPDATE product SET is_published = " + is_published + " Where p_id=" + item + "; "
        });

        db.query(str, next);

    } else {

        is_published = 0;


        var str = "";
        data.forEach(function(item) {

            console.log("item", item);
            str += "UPDATE product SET is_published = " + is_published + " Where p_id=" + item + "; "
        });

        db.query(str, next);

    }


};

exports.getPublish = function getPublish(data, next) {
    //code goes here.
    console.log("getPublish");
    db.query("SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_published =\'' + 1 + '\' AND is_archived =\'' + 0 + '\' ORDER BY start_date DESC;", next);
    /*var query = 'SELECT * from product where is_published =\'' + 1 + '\'';
    db.query(query, next);*/

};

exports.getUnpublish = function getUnpublish(data, next) {
    //code goes here.


    db.query("SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_published =\'' + 0 + '\' AND is_archived =\'' + 0 + '\' ORDER BY start_date DESC;", next);
    /*var query = 'SELECT * from product where is_published =\'' + 0 + '\'';
    db.query(query, next);*/

};


exports.getActive = function getActive(data, next) {
    //code goes here.
    db.query("SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_archived =\'' + 0 + '\' ORDER BY start_date DESC;", next);
    /*var query = 'SELECT * from product where is_archived =\'' + 0 + '\'';
    db.query(query, next);*/

};

exports.getArchive = function getArchive(data, next) {
    //code goes here.
    db.query("SELECT pv.product_imgPath, p.p_id, p.item_name, pt.name, p.sell_price, p.is_published, p.start_date FROM product p LEFT JOIN attribute_value av ON av.product_id = p.p_id LEFT JOIN product_variant pv ON pv.prod_id = p.p_id  LEFT JOIN product_type pt ON p.productType_id = pt.ptype_id where is_archived =\'' + 1 + '\' ORDER BY start_date DESC;", next);
    /*var query = 'SELECT * from product where is_archived =\'' + 1 + '\'';
    db.query(query, next);*/

};
