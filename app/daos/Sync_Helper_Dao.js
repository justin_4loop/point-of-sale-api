'use strict';
var mysql = require('mysql');
var async = require('async');
var _ = require('lodash-node');
var archiver = require('archiver');
var fs = require('fs-extra');
var path = require('path');
var Database = require('../../app/utils/database').Database;
var db = new Database();

exports.UpdateProductLogsStat = function UpdateProductLogsStat(params, store_id, devicename, modLogs, next) {
    switch (modLogs) {
        case "PRODUCTLOGS":
            async.waterfall([
                function(cb) {
                    var qry = mysql.format('UPDATE prod_device_logs dl ' +
                        'JOIN product_logs pl ON pl.pl_id = dl.pl_id set dl.dlstatus = 1 ' +
                        'WHERE pl.tbl_pk_id IN (?) AND pl.store_id = ? AND dl.device_id = ?;', [params.id, store_id, devicename]);

                    db.query(qry, function(err, resqry) {
                        cb(null, resqry);
                    });
                },
                function(resqry, cb) {
                    for (var i = 0; i < params.id.length; i++) {
                        async.waterfall([
                            function(cb) {
                                var insideQry = mysql.format('SELECT (case WHEN ' +
                                    '(SELECT count(dl.device_id) FROM prod_device_logs dl ' +
                                    'JOIN product_logs pl ON pl.pl_id = dl.pl_id WHERE pl.store_id = ? AND pl.tbl_pk_id = ?) ' +
                                    '=  ' +
                                    '(SELECT count(dl.device_id) FROM prod_device_logs dl ' +
                                    'JOIN product_logs pl ON pl.pl_id = dl.pl_id WHERE pl.store_id = ? AND pl.tbl_pk_id = ? AND dl.dlstatus = ?) ' +
                                    'then true ELSE false END) as output;', [store_id, params.id[i], store_id, params.id[i], 1]);
                                var increment = params.id[i]; // assign params to increment variable bcoz the value got undefined after db.query.
                                db.query(insideQry, function(err, insideQry) {
                                    cb(null, insideQry, increment);
                                });
                            },
                            function(insideQry, paramsid, cb) {
                                var iQry = mysql.format('UPDATE product_logs pl JOIN prod_device_logs dl ON dl.pl_id = pl.pl_id ' +
                                    'SET pl.status = 1 WHERE pl.store_id = ? AND pl.tbl_pk_id = ?;', [store_id, paramsid]);

                                if (insideQry[0].output == 1) {
                                    db.query(iQry, function(err, resiQry) {
                                        if (err) {
                                            console.log('ERROR :', err);
                                        }
                                    });
                                }
                            }
                        ]);
                    }
                    cb(null, 'All good.');
                }
            ], next);
            break;
        case "EMPLOYEELOGS":
            async.waterfall([
                function(cb) {
                    var qry = mysql.format('UPDATE emp_device_logs edl ' +
                        'JOIN employee_logs epl ON epl.el_id = edl.el_id set edl.edlstatus = 1 ' +
                        'WHERE epl.tbl_pk_id IN (?) AND epl.store_id = ? AND edl.device_id = ?;', [params.id, store_id, devicename]);

                    db.query(qry, function(err, resqry) {
                        cb(null, resqry);
                    });
                },
                function(resqry, cb) {
                    for (var i = 0; i < params.id.length; i++) {
                        async.waterfall([
                            function(cb) {
                                var insideQry = mysql.format('SELECT (case WHEN ' +
                                    '(SELECT count(edl.device_id) FROM emp_device_logs edl ' +
                                    'JOIN employee_logs epl ON epl.el_id = edl.el_id WHERE epl.store_id = ? AND epl.tbl_pk_id = ?) ' +
                                    '=  ' +
                                    '(SELECT count(edl.device_id) FROM emp_device_logs edl ' +
                                    'JOIN employee_logs epl ON epl.el_id = edl.el_id WHERE epl.store_id = ? AND epl.tbl_pk_id = ? AND edl.edlstatus = ?) ' +
                                    'then true ELSE false END) as output;', [store_id, params.id[i], store_id, params.id[i], 1]);
                                var increment = params.id[i]; // assign params to increment variable bcoz the value got undefined after db.query.
                                db.query(insideQry, function(err, insideQry) {
                                    cb(null, insideQry, increment);
                                });
                            },
                            function(insideQry, paramsid, cb) {
                                var iQry = mysql.format('UPDATE employee_logs epl JOIN emp_device_logs edl ON edl.el_id = epl.el_id ' +
                                    'SET epl.status = 1 WHERE epl.store_id = ? AND epl.tbl_pk_id = ?;', [store_id, paramsid]);
                                if (insideQry[0].output == 1) {
                                    db.query(iQry, function(err, resiQry) {
                                        if (err) {
                                            console.log('ERROR :', err);
                                        }
                                    });
                                }
                            }
                        ]);
                    }
                    cb(null, 'All good.');
                }
            ], next);
            break;
        default:
            break;
    }
};

exports.getLogs = function getLogs(devicename, store_id, tblname, tblTrans, modLogs, cb) {
    var qryLogs;

    switch (modLogs) {
        case "PRODUCTLOGS":
            if (store_id != null) {
                qryLogs = mysql.format('SELECT tbl_pk_id FROM product_logs pl INNER ' +
                    'JOIN prod_device_logs dl ON dl.pl_id = pl.pl_id WHERE dl.device_id = ? AND dl.dlstatus = 0  AND  ' +
                    'pl.store_id = ?  AND pl.transaction = ? AND pl.table_name = ?;', [devicename, store_id, tblTrans, tblname]);
            } else {
                qryLogs = mysql.format('SELECT tbl_pk_id FROM product_logs pl INNER ' +
                    'JOIN prod_device_logs dl ON dl.pl_id = pl.pl_id WHERE dl.device_id = ? AND dl.dlstatus = 0  AND  ' +
                    'pl.transaction = ? AND pl.table_name = ?;', [devicename, tblTrans, tblname]);
            }
            break;
        case "EMPLOYEELOGS":
            qryLogs = mysql.format('SELECT tbl_pk_id FROM employee_logs epl INNER ' +
                'JOIN emp_device_logs edl ON edl.el_id = epl.el_id WHERE edl.device_id = ? AND edl.edlstatus = 0  AND  ' +
                'epl.store_id = ?  AND epl.transaction = ? AND epl.table_name = ?;', [devicename, store_id, tblTrans, tblname]);

            break;
        default:
            break;
    }

    db.query(qryLogs, function(err, logs) {
        if (err) {
            console.log('ERROR :', err);
        }

        var arrlogs = [];
        _.each(logs, function(_l) {
            arrlogs.push(_l.tbl_pk_id);
        });

        arrlogs.length != 0 ? arrlogs : arrlogs.push(-99999);

        cb(null, arrlogs);

    });
};

exports.ZipImages = function ZipImages(params, devicename, tabletype, modLogs, cb) {

    var archive = archiver('zip');
    var output = fs.createWriteStream("./public/tmp/" + devicename + ".zip");
    var imageArr = [];

    switch (modLogs) {
        case "PRODUCTLOGS":
            if (tabletype == "product_type") {
                _.each(params, function(prod_var) {
                    if (fs.existsSync(prod_var.ptype_image)) {
                        imageArr.push(path.basename(prod_var.ptype_image));
                    }
                });
            } else if (tabletype == "product_variant") {
                _.each(params, function(prod_var) {
                    if (fs.existsSync(prod_var.product_imgPath)) {
                        imageArr.push(path.basename(prod_var.product_imgPath));
                    }
                });
            }
            break;
        case "EMPLOYEELOGS":
            if(tabletype == "employees"){
                _.each(params, function(emp){
                    if(fs.existsSync(emp.image)){
                        imageArr.push(path.basename(emp.image));
                    }
                });
            }
            break;
        default:
            break;
    }

    output.on('close', function() {
        console.log('archiver has been finalized and the output file descriptor has closed.');
        cb(null, 'Successully zip the images.');
    });

    archive.on('error', function(err) {
        throw err;
    });

    if (!fs.existsSync('./public/tmp')) { // if directory not exist create one .
        fs.ensureDirSync('./public/tmp');
    }

    archive.pipe(output);

    archive.bulk([{
        expand: true,
        cwd: './public/uploads/img/',
        src: imageArr
    }]);

    archive.finalize();
};
