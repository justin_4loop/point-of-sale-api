'use strict';
var mysql = require('mysql');
var async = require('async');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var _ = require('lodash-node');

exports.savePriceHistory = function savePriceHistory(id, dataParams, next) {
    async.waterfall([
        function(cb) {
            if (dataParams.length > 0) {
                var str = mysql.format('DELETE FROM price_history WHERE fk_product_id =\'' + id + '\';');
                db.query(str, function(err, data) {
                    if (err) {
                        console.log('Error on DELETE', err);
                        cb(err, null);
                    }
                    cb(null, data);
                });
            } else {
                cb(null, 'Proceed to next');
            }
        },
        function(mesg, cb) {
            var dataArr = [];
            _.each(dataParams, function(item) {
                var temp = [];
                temp.push(item.store_id);
                temp.push(item.fk_product_id);
                temp.push(item.base_price);
                temp.push(item.regular_price);
                temp.push(item.rrp);
                temp.push(item.sale_price);
                temp.push(item.sale_percent);
                dataArr.push(temp);
            });

            var str = mysql.format('INSERT INTO price_history(store_id,fk_product_id,base_price,regular_price,rrp,sale_price,sale_percent) VALUES ?', [dataArr]);
            console.log('str: ', str);
            db.query(str, function(err, data) {
                if (err) {
                    console.log('Error on insert', err);
                    cb(err, null);
                }
                console.log('data: ', data);
                cb(null, data);
            });
        }
    ], next);
};

exports.getPriceHistory = function getPriceHistory(data, next) {
    //code goes here.

    db.query('SELECT * FROM price_history', next);
};

exports.getStoreOwner = function getStoreOwner(id, next) {
    var str = mysql.format('SELECT price.ph_id as ph_id, store.store_id AS store_id, store.name AS name, store.address AS address, store.owner_name AS owner_name FROM store_info AS store JOIN price_history AS price ON store.store_id = price.store_id WHERE fk_product_id =?;', [id]);
    db.query(str, next);
};

exports.getSpecificPriceHistory = function getSpecificPriceHistory(id, data, next) {
    //code goes here.
    db.query('SELECT store.store_id,price.ph_id,price.sale_price,price.sale_percent,price.base_price,price.rrp,price.regular_price,price.fk_product_id FROM store_info AS store JOIN price_history AS price ON store.store_id = price.store_id WHERE fk_product_id =\'' + id + '\';', next);
};


exports.deletePriceHistory = function deletePriceHistory(id, next) {
    //code goes here.

    var query = 'DELETE FROM price_history WHERE ph_id =\'' + id + '\'';
    db.actionQuery(query, next);

};


exports.updatePriceHistory = function updatePriceHistory(id, data, next) {
    console.log("data", data);
    console.log("id", id);

    var str = "";
    data.forEach(function(item) {
        str += "UPDATE price_history SET sale_price = " + item.sale_price + " ,sale_percent=" + item.sale_percent + ",fk_product_id=" + item.fk_product_id + " Where ph_id=" + item.ph_id + "; "
    });

    db.query(str, next);
};
