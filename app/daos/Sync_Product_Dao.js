'use strict';
var mysql = require('mysql');
var async = require('async');
var _ = require('lodash-node');
var archiver = require('archiver');
var fs = require('fs-extra');
var path = require('path');
var Database = require('../../app/utils/database').Database;
var Helper = require('../daos/Sync_Helper_Dao');
var db = new Database();

/** ***************************  P R O D U C T *************************  **/

exports.getProduct = function getProduct(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "product", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT p.* FROM product p JOIN inventory i ON i.fk_p_id = p.p_id ' +
            'JOIN store_info si ON si.store_id = i.fk_store_id ' +
            'WHERE p.p_id IN (?) AND si.store_id = 68 GROUP BY p.p_id;', [logs]);

        db.query(qry, next);
    });
};

exports.getProduct_Variant = function getProduct_Variant(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "product_variant", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT pv.* FROM product_variant pv ' +
            'JOIN product p ON p.p_id = pv.prod_id ' +
            'JOIN inventory i ON i.fk_p_id = p.p_id ' +
            'JOIN store_info si ON si.store_id = i.fk_store_id WHERE pv.pv_id IN (?) AND si.store_id = ? GROUP BY pv.pv_id;', [logs, store_id]);

        db.query(qry, function(err, data) {
            Helper.ZipImages(data, devicename, "product_variant", "PRODUCTLOGS", function(err, zipres) {
                next(null, data);
            });
        });
    });
};

exports.getProductType = function getProductType(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "product_type", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT pt.* FROM product_type pt ' +
            'JOIN product p ON p.productType_id = pt.ptype_id ' +
            'JOIN inventory i ON i.fk_p_id = p.p_id ' +
            'JOIN store_info si ON si.store_id = i.fk_store_id WHERE pt.ptype_id IN (?) AND si.store_id = ? GROUP BY pt.ptype_id;', [logs, store_id])

        db.query(qry, function(err, data) {
            Helper.ZipImages(data, devicename, "product_type", "PRODUCTLOGS", function(err, zipres) {
                next(null, data);
            });
        });
    });
};

exports.getAttributeValue = function getAttributeValue(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "attribute_value", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT av.* FROM attribute_value av ' +
            'JOIN product p ON av.product_id = p.p_id ' +
            'JOIN inventory i ON i.fk_p_id = p.p_id ' +
            'JOIN store_info si ON si.store_id = i.fk_store_id ' +
            'WHERE av.prod_att_id IN (?) AND si.store_id = ? GROUP BY av.prod_att_id;', [logs, store_id]);

        db.query(qry, next);
    });
};

exports.getInventory = function getInventory(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "inventory", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT iv.* FROM inventory iv ' +
            'JOIN store_info si ON si.store_id = iv.fk_store_id ' +
            'WHERE iv.inv_id IN (?) AND si.store_id = ? GROUP BY iv.inv_id;', [logs, store_id]);

        db.query(qry, next);
    });
};

exports.getPriceHistory = function getPriceHistory(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "price_history", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT ph.* FROM price_history ph ' +
            'JOIN store_info si ON si.store_id = ph.store_id WHERE ph.ph_id IN (?) AND si.store_id = ? GROUP BY ph.ph_id;', [logs, store_id]);

        db.query(qry, next);
    });
};


exports.getAttributes = function getAttributes(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "attributes", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT * FROM attributes a WHERE a.att_id IN (?) ;', [logs]);

        db.query(qry, next);
    });
};

exports.getAttributeGroup = function getAttributeGroup(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "attribute_group", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT * FROM attribute_group a WHERE a.attgrp_id IN (?) ;', [logs]);

        db.query(qry, next);
    });
};

exports.getAttributeCategoryMap = function getAttributeCategoryMap(store_id, devicename, next) {
    Helper.getLogs(devicename, store_id, "attribute_category_map", "INSERT", "PRODUCTLOGS", function(err, logs) {
        var qry = mysql.format('SELECT * FROM attribute_category_map a WHERE a.attcat_id IN (?) ;', [logs]);

        db.query(qry, next);
    });
};


/** *************************** E N D  P R O D U C T *************************  **/
